//
//  CUStorage.h
//  CelcoinUsuario
//
//  Created by Adya on 7/13/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

// Define keys for storage

#import <Foundation/Foundation.h>
#import "TSStorage.h"

// Used to store phone number when registering user to send sms to that phone later.
#define STORAGE_VALUE_PHONE @"UserPhoneNumber"
// Used to store sms code entered during registration process
#define STORAGE_VALUE_SMS_PASSWORD @"UserSMSPasswordConfirmation"
// Used to store date of the sms entered during registration process
#define STORAGE_VALUE_SMS_DATE @"UserSMSDateConfirmation"

#define STORAGE_VALUE_PAYMENT_REVIEW @"PaymentReview"

#define STORAGE_VALUE_CONTACT_TRANSFER @"TransferContact"

#define STORAGE_CONST_ARRAY_OPERATORS_NAMES @[@"TIM", @"CLARO", @"VIVO", @"OI", @"CTBC", @"NEXTEL"]
#define STORAGE_CONST_ARRAY_OPERATORS_CODES @[@2086, @2087, @2088, @2089, @2093, @2098]

#define STORAGE_ARRAY_BANKS_LIST @"BanksList"

#define STORAGE_VALUE_SUCCESS_TITLE @"SuccessTitle"

#define STORAGE_VALUE_SUCCESS_MESSAGE @"SuccessMessage"

#define STORAGE_VALUE_WEB_URL @"WebViewUrl"
#define STORAGE_VALUE_WEB_TITLE @"WebViewTitle"
#define STORAGE_FLAG_WEB_MENU @"WebViewMenu"

// Reference to the main menu view controller (to be able to pop back to the menu)
#define STORAGE_REF_MENU_VIEW_CONTROLLER @"RefCUMainMenuViewController"

#define STORAGE_FLAG_REG_COMPLETE @"FlagCompleteReg"
