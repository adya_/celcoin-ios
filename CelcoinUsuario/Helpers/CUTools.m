//
//  CUTools.m
//  CelcoinUsuario
//
//  Created by Adya on 7/14/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUTools.h"
#import "TSUtils.h"


#define PHONE_COUNTRY_CODE @"55"
#define REGEX_PHONE_PATTERN @"^("PHONE_COUNTRY_CODE@")?(0?\\d{2})?(\\d{4,5})(\\d{4})$"
#define FORMAT_PRETTY_PHONE_PATTERN @"+%@ (%@) %@-%@"
#define FORMAT_SHORT_PRETTY_PHONE_PATTERN @"(%@) %@-%@"
#define FORMAT_MIN_PRETTY_PHONE_PATTERN @"%@-%@"
#define FORMAT_PHONE_PATTERN @"+%@%@%@%@"
#define FORMAT_SHORT_PHONE_PATTERN @"%@%@%@"
#define FORMAT_MIN_PHONE_PATTERN @"%@%@"


@implementation CUTools

+(NSString*) extractNumber:(NSString *)numberString{
    return [[numberString componentsSeparatedByCharactersInSet:
             [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
            componentsJoinedByString:@""];;
}


+(BOOL) isValidPhone:(NSString*) numberString{
    numberString = [self extractNumber:numberString];
    NSPredicate* phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", REGEX_PHONE_PATTERN];
    return ([phoneTest evaluateWithObject:numberString]);
    //&& !(numberString.length == 11 && [numberString rangeOfString:PHONE_COUNTRY_CODE].location == 0)); // contry code not at the beggining
}

+(NSString*) prettyNumber:(NSString *)numberString{
    if (!numberString) return nil;
    numberString = [self extractNumber:numberString];
    if (numberString.length == 11 && [numberString rangeOfString:PHONE_COUNTRY_CODE].location == 0)
        numberString = [numberString substringFromIndex:2];
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:REGEX_PHONE_PATTERN options:0 error:nil];
    
    if (numberString.length == 9 || numberString.length == 8)
        return [regex stringByReplacingMatchesInString:numberString options:0 range:NSMakeRange(0, numberString.length) withTemplate:[NSString stringWithFormat:FORMAT_MIN_PRETTY_PHONE_PATTERN, @"$3",@"$4"]];
    else{
        return [regex stringByReplacingMatchesInString:numberString options:0 range:NSMakeRange(0, numberString.length) withTemplate:[NSString stringWithFormat:FORMAT_PRETTY_PHONE_PATTERN, PHONE_COUNTRY_CODE,@"$2", @"$3",@"$4"]];
    }
}



+(NSString*) formattedNumber:(NSString*)numberString{
    if (!numberString) return nil;
    numberString = [self extractNumber:numberString];
    // remove country code if needed
    if (numberString.length == 11 && [numberString rangeOfString:PHONE_COUNTRY_CODE].location == 0)
        numberString = [numberString substringFromIndex:2];

    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:REGEX_PHONE_PATTERN options:0 error:nil];
    if (numberString.length == 9)
        return [regex stringByReplacingMatchesInString:numberString options:0 range:NSMakeRange(0, numberString.length) withTemplate:[NSString stringWithFormat:FORMAT_MIN_PHONE_PATTERN, @"$3",@"$4"]];
    else{
        return [regex stringByReplacingMatchesInString:numberString options:0 range:NSMakeRange(0, numberString.length) withTemplate:[NSString stringWithFormat:FORMAT_PHONE_PATTERN, PHONE_COUNTRY_CODE,@"$2", @"$3",@"$4"]];
    }
}

+(NSString*) formattedLocalNumber:(NSString *)numberString{
    NSString* number = [self formattedNumber:numberString];
    if ([number rangeOfString:@"+"].location == 0)
        number = [number substringFromIndex:(PHONE_COUNTRY_CODE.length + 1)];
    return number;
}
@end
