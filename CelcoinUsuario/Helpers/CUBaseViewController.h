//
//  CUMenuItemsProvider.h
//  CelcoinUsuario
//
//  Created by Adya on 7/2/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSMenuManager.h"
#import "TSViewController.h"

@interface CUBaseViewController : TSViewController <TSMenuManagerItemsProvider>

-(void) showSuccessViewController;

-(NSString*) stringWithCurrency:(float) value;

@end
