//
//  CUMenuItemsProvider.m
//  CelcoinUsuario
//
//  Created by Adya on 7/2/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUBaseViewController.h"
#import "REMenuItem.h"
#import "REMenu.h"
#import "CUPerguntasFrequentesViewController.h"
#import "CUFaleConoscoViewController.h"
#import "TSUtils.h"
#import "CUSuccessViewController.h"
#import "CULoginManager.h"
#import "TSNotifier.h"

@interface CUBaseViewController () <CULoginCallback>

@end

@implementation CUBaseViewController{
    CUPerguntasFrequentesViewController* faq;
    CUFaleConoscoViewController* conosco;
}

-(void) viewDidLoad{
    [super viewDidLoad];
    self.menuManager.itemsProvider = self;
    [self customizeMenu];
    [self initMenuViewControllers];
    [CULoginManager sharedManager].delegate = self;
    [self setBackButtonWithTitle:NSLocalizedString(@"CUN_NAV_BACK_BUTTON", nil)];
}

-(void) customizeMenu{
    self.menuButton.tintColor = [UIColor whiteColor];
    [self setMenuButtonWithImage:[UIImage imageNamed:@"btn_menu"]];
    REMenu* menu = self.menuManager.menu;
    menu.backgroundColor = colorRGB(250, 250, 250);
    menu.textColor = colorRGB(232, 0, 48);
    menu.font = [UIFont systemFontOfSize:18];
    menu.highlightedBackgroundColor = colorRGB(240, 230, 230);
    menu.highlightedTextColor = colorRGB(222, 0, 32);
    menu.separatorHeight = 1.0f;
    menu.separatorColor = colorARGB(125, 232, 0, 48);
    menu.highlightedSeparatorColor = menu.separatorColor;
    
    menu.borderWidth = 0;
//    menu.borderColor = menu.separatorColor;
    menu.shadowColor = colorARGB(0, 0, 0, 0);
    menu.textShadowColor = colorARGB(0, 0, 0, 0);
    menu.highlightedTextShadowColor = colorARGB(0, 0, 0, 0);
}

-(void) initMenuViewControllers{
    faq  = [self.storyboard instantiateViewControllerWithIdentifier:@"CUFAQ"];
    conosco = [self.storyboard instantiateViewControllerWithIdentifier:@"CUContactUs"];
}

-(void) showSuccessViewController{
    CUSuccessViewController* controller = [self.storyboard instantiateViewControllerWithIdentifier:@"CUSuccess"];
    [self showSafeViewController:controller sender:self];
}


-(NSString*) stringWithCurrency:(float) value{
    return [NSString stringWithFormat:@"R$ %.2f", value];
}

-(NSArray*) getMenuItemsForViewController:(UIViewController*)controller{

//    REMenuItem* item1 = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_MINHA_CONTA", @"menu My Account") image:nil highlightedImage:nil action:^(REMenuItem *item) {
//        NSLog(@"No account view.");
//    }];
    REMenuItem* item2 = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_FALE_CONOSCO", @"menu Contact Us") image:nil highlightedImage:nil action:^(REMenuItem *item) {
        [self showSafeViewController:conosco sender:controller];    }];
    
    REMenuItem* item3 = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_PERGUNTAS_FREQUENTES", @"menu F.A.Q.") image:nil highlightedImage:nil action:^(REMenuItem *item) {
        [self showSafeViewController:faq sender:controller];
    }];
    
    REMenuItem* item4 = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_SAIR", @"menu Logout") image:nil highlightedImage:nil action:^(REMenuItem *item) {
        [[CULoginManager sharedManager] performLogout];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    return @[/*item1,*/ item2, item3, item4];
}

-(void) onLogoutResult:(BOOL)success withError:(TSError *)error{
    if (success)
        [TSNotifier log:@"Logout complete."];
}

@end
