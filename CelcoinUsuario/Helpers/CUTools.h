//
//  CUTools.h
//  CelcoinUsuario
//
//  Created by Adya on 7/14/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CUTools : NSObject

+(NSString*) extractNumber:(NSString *)numberString;

+(BOOL) isValidPhone:(NSString*) numberString;

// +xxxxxxxxxxxxx
+(NSString*) formattedNumber:(NSString*) numberString;

// xxxxxxxxxxx
+(NSString*) formattedLocalNumber:(NSString *)numberString;

// +xx (xx) xxxxx-xxxx
+(NSString*) prettyNumber:(NSString*) numberString;
@end
