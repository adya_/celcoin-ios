//
//  CURecargaJogosViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/2/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CURecargaJogosViewController.h"
#import "TSNotifier.h"

@interface CURecargaJogosViewController ()

@end

@implementation CURecargaJogosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)go4GoldClick:(id)sender {
    [TSNotifier alertWithTitle:NSLocalizedString(@"CUN_TITLE_MY_CELCOIN", @"My Celcoin") message:NSLocalizedString(@"CUN_MSG_MY_CELCOIN", @"Message My Celcoin")];
}
- (IBAction)paymentezClick:(id)sender {
    [TSNotifier alertWithTitle:NSLocalizedString(@"CUN_TITLE_MY_CELCOIN", @"My Celcoin") message:NSLocalizedString(@"CUN_MSG_MY_CELCOIN", @"Message My Celcoin")];
}
- (IBAction)levelupClick:(id)sender {
    [TSNotifier alertWithTitle:NSLocalizedString(@"CUN_TITLE_MY_CELCOIN", @"My Celcoin") message:NSLocalizedString(@"CUN_MSG_MY_CELCOIN", @"Message My Celcoin")];
}

@end
