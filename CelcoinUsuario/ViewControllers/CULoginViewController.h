//
//  LoginViewController.h
//  CelcoinUsuario
//
//  Created by Adya on 6/21/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUBaseViewController.h"

@interface CULoginViewController : CUBaseViewController
@property (weak, nonatomic) IBOutlet UITextField *tfLogin;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;

@end
