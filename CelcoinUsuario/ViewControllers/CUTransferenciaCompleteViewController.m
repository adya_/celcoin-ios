//
//  CUTransferenciaCompleteViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/3/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUTransferenciaCompleteViewController.h"
#import "TSCurrencyTextField.h"
#import "TSContactsManager.h"
#import "CUStorage.h"
#import "TSUtils.h"
#import "TSNotifier.h"
#import "CUTools.h"
#import "CUWalletManager.h"

@interface CUTransferenciaCompleteViewController () <UITableViewDelegate, UITableViewDataSource, CUWalletCallback>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet TSCurrencyTextField *tfValue;

@end

@implementation CUTransferenciaCompleteViewController{
    TSContact* contact;
    int selectedNumber;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showMenuButton = NO;
    [CUWalletManager sharedManager].delegate = self;
    selectedNumber = -1;
    contact = ((TSContact*)[[TSStorage sharedStorage] getValueForKey:STORAGE_VALUE_CONTACT_TRANSFER]);
    
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return contact.auxPhones.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    NSString* number = ((TSPhone*)[contact.auxPhones objectAtIndex:indexPath.row]).number;
    if ([CUTools isValidPhone:number])
        cell.textLabel.text = [CUTools prettyNumber:number];
    else
        cell.textLabel.text = number;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    if (selectedNumber == indexPath.row){
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        cell.accessoryType = UITableViewCellAccessoryNone;
        selectedNumber = -1;
    }
    else {
        TSPhone* phone = [contact.auxPhones objectAtIndex:indexPath.row];
        UIView *bgColorView = [UIView new];
        if ([CUTools isValidPhone:phone.number]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        bgColorView.backgroundColor = colorRGB(210, 240, 210);
            selectedNumber = (int)indexPath.row;
            [cell setSelectedBackgroundView:bgColorView];
        }
        else{
             bgColorView.backgroundColor = colorRGB(240, 210, 210);
            selectedNumber = -1;
            [cell setSelectedBackgroundView:bgColorView];
            [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
            cell.accessoryType = UITableViewCellAccessoryNone;
            [TSNotifier notify:NSLocalizedString(@"CUN_PHONE_CANT_BE_USED_TRANSFERENCIA", nil) onView:self.view forTimeInterval:TS_NOTIFICATION_TIME_SHORT];
        } 
    }
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
    [cell setSelectedBackgroundView:nil];
    selectedNumber = -1;
}

- (IBAction)completeClick:(id)sender {
    float value = [self.tfValue.amount floatValue];
    
    if (value == 0)
        [TSNotifier notify:NSLocalizedString(@"CUN_SET_UP_VALUE_TRANSFERENCIA", nil) onView:self.view];
    else if (selectedNumber == -1)
        [TSNotifier notify:NSLocalizedString(@"CUN_SELECT_PHONE_TO_TRANSFER", nil) onView:self.view];
    else{
        NSString* phone = ((TSPhone*)[contact.auxPhones objectAtIndex:selectedNumber]).number;
        if ([CUTools isValidPhone:phone]){
            phone = [CUTools formattedNumber:phone];
            NSLog(@"phone = %@", phone);
            NSString* msg = [NSString stringWithFormat:NSLocalizedString(@"CUN_CONFIRM_TRANSFERENCIA_MESSAGE", nil), contact.fullName, phone, value];
            
            [TSNotifier alertWithTitle:NSLocalizedString(@"CUN_CONFIRM_TRANSFERENCIA", nil) message:msg withAlignment:NSTextAlignmentLeft acceptButton:NSLocalizedString(@"CUN_YES", nil) acceptBlock:^{
                [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_TRANSFERENCIA_IN_PROGRESS", nil)];
                [[CUWalletManager sharedManager] performTransfer:value toPhoneWithNumber:phone];
            } cancelButton:NSLocalizedString(@"CUN_NO", nil) cancelBlock:nil];
        }
       
    }
}

-(void) onWalletTransactionResult:(BOOL)success withProofMessage:(NSString *)message orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [[TSStorage sharedStorage] putValue:message forKey:STORAGE_VALUE_SUCCESS_MESSAGE];
        [[TSStorage sharedStorage] putValue:NSLocalizedString(@"CUN_MESSAGE_SUCCESS_SAQUE", @"Saque success") forKey:STORAGE_VALUE_SUCCESS_TITLE];
        [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_UPDATING_WALLET", @"updating wallet")];
        [[CUWalletManager sharedManager] performUpdateWallet];
    }
    else{
        [TSNotifier alertError:error];
    }
}

-(void) onWalletUpdateResult:(BOOL)success withSelectedWallet:(CUWallet *)wallet orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    [self showSuccessViewController];
}

@end
