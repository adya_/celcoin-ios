//
//  CUSaqueCompleteViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/3/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUSaqueCompleteViewController.h"
#import "CUBankInfo.h"
#import "CUWalletManager.h"
#import "ActionSheetStringPicker.h"
#import "CUStorage.h"
#import "TSNotifier.h"

@interface CUSaqueCompleteViewController () <CUWalletCallback>
@property (weak, nonatomic) IBOutlet UITextField *tfNickname;
@property (weak, nonatomic) IBOutlet UIButton *bBank;
@property (weak, nonatomic) IBOutlet UITextField *tfAgency;
@property (weak, nonatomic) IBOutlet UITextField *tfAccount;
@property (weak, nonatomic) IBOutlet UITextField *tfVerification;
@property (weak, nonatomic) IBOutlet UISegmentedControl *scAccountType;

@end

@implementation CUSaqueCompleteViewController{
    NSArray* banksList;
    NSUInteger selectedBank;
    CUBankInfo* editedBankInfo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showMenuButton = NO;
    banksList = [[TSStorage sharedStorage] getValueForKey:STORAGE_ARRAY_BANKS_LIST];
    self.bBank.titleLabel.numberOfLines = 1;
    self.bBank.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.bBank.titleLabel.lineBreakMode = NSLineBreakByClipping;
}

-(void)  viewWillAppear:(BOOL)animated{
    CUBankInfo* bankInfo = [CUWalletManager sharedManager].bankReference;
    if (bankInfo){
        self.tfNickname.text = bankInfo.description;
        [self.bBank setTitle:bankInfo.bankDescription forState:UIControlStateNormal];
        self.tfAgency.text = [NSString stringWithFormat:@"%d", bankInfo.agency];
        self.tfAccount.text = [NSString stringWithFormat:@"%d", bankInfo.account];
        self.tfVerification.text = bankInfo.verificationDigit;
        [self.scAccountType setSelectedSegmentIndex:bankInfo.bankAccountType-1];
        selectedBank = [banksList indexOfObject:bankInfo.bankDescription];
    }
    else{
        selectedBank = -1;
        [self.scAccountType setSelectedSegmentIndex:0]; // Conta Corrente Default
    }
    [CUWalletManager sharedManager].delegate = self;
    
    editedBankInfo = [[CUBankInfo alloc] initWithBankInfo:bankInfo];
}


- (IBAction)selectBankClick:(id)sender {
    [self dismissKeyboard];
    NSUInteger initBank = (selectedBank == -1 ? 0 : selectedBank);
    ActionSheetStringPicker* picker = [ActionSheetStringPicker showPickerWithTitle:@"Selecione o banco" rows:banksList initialSelection:initBank doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        selectedBank = selectedIndex;
        [self.bBank setTitle:selectedValue forState:UIControlStateNormal];
    } cancelBlock: nil origin:self.view];
    picker.tapDismissAction = TapActionCancel;
    picker.pickerText = [UIFont systemFontOfSize:12];
}

- (IBAction)completeClick:(id)sender {
    [self dismissKeyboard];
    editedBankInfo.description = self.tfNickname.text;
    editedBankInfo.agency = [self.tfAgency.text intValue];
    editedBankInfo.account = [self.tfAccount.text intValue];
    editedBankInfo.verificationDigit = self.tfVerification.text;
    editedBankInfo.bankDescription = [banksList objectAtIndex:selectedBank];
    editedBankInfo.bankID = [[editedBankInfo.bankDescription substringWithRange:NSMakeRange(0, 3)] intValue];
    editedBankInfo.bankAccountType = (int)self.scAccountType.selectedSegmentIndex + 1;
    
    if (![editedBankInfo isEqual:[CUWalletManager sharedManager].bankReference]){
        if (self.tfNickname.text.length == 0 ||
            self.tfAccount.text.length == 0 ||
            self.tfAgency.text.length == 0 ||
            selectedBank == -1
            )
            [TSNotifier notify:NSLocalizedString(@"CUN_SAQUE_NOT_ALL_FIELDS_FILLED", nil) onView:self.view];
        else{
            [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_SAQUE_SAVING_BANK_INFO", nil)];
            [[CUWalletManager sharedManager] performSetBankReference:editedBankInfo];
        }
        
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void) onWalletBankReferenceSavedResult:(BOOL)success orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success)
        [self.navigationController popViewControllerAnimated:YES];
    else{
        [TSNotifier alertError:error];
    }
}

@end
