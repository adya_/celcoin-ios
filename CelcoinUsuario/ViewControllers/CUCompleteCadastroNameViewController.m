//
//  CUCompleteCadastroNameViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 8/8/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUCompleteCadastroNameViewController.h"
#import "TSNotifier.h"
#import "CULoginManager.h"
#import "TSUtils.h"
#import "CUStorage.h"
#import "REFormattedNumberField.h"
#import "CUTools.h"

@interface CUCompleteCadastroNameViewController () <CULoginCallback, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet REFormattedNumberField *tfCPF;
@property (weak, nonatomic) IBOutlet UITextField *tfName;

@end

@implementation CUCompleteCadastroNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showMenuButton = NO;
    [self.tfCPF setFormat:@"XXX.XXX.XXX-XX"];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 100;
}

- (IBAction)backClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)completeClick:(id)sender {
    NSString* cpf = [CUTools extractNumber:self.tfCPF.text];
    NSString* name = trim(self.tfName.text);
    if (cpf.length == 0 || name.length == 0){
        [TSNotifier notify:NSLocalizedString(@"CUN_FILL_BOTH_FIELDS", nil) onView:self.view ];
        return;
    }
    [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_COMPLETE_REGISTRATION_IN_PROGRESS", nil)];
    [[CULoginManager sharedManager] performCompleteRegistrationWithName:name andCPF:cpf];
}



-(void) onRegistrationResult:(BOOL)success withError:(TSError *)error{
    
    if (success){
        [[CULoginManager sharedManager] performUpdateAccountInfo];
    }
    else{
        [TSNotifier hideProgressOnView:self.view];
        [TSNotifier alertError:error];
    }
}

-(void) onRegistrationInfoResult:(BOOL)success withInfo:(CUUserInfo *)info orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [[TSStorage sharedStorage] putValue:@(YES) forKey:STORAGE_FLAG_REG_COMPLETE];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [TSNotifier alertError:error];
    }
}

@end
