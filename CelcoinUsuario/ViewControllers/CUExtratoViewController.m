//
//  CUExtratoViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/2/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUExtratoViewController.h"
#import "CUWalletManager.h"
#import "CUWallet.h"
#import "ActionSheetDatePicker.h"
#import "TSUtils.h"
#import "CUHistoryManager.h"
#import "CUTransactionCell.h"
#import "TSNotifier.h"

#define PRELOAD_DAYS_HISTORY 10

@interface CUExtratoViewController () <UITableViewDelegate, UITableViewDataSource, CUHistoryCallback, CUWalletCallback>
@property (weak, nonatomic) IBOutlet UILabel *lSaldo;
@property (weak, nonatomic) IBOutlet UIButton *bUpdateWallet;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivUpdateWallet;

@property (weak, nonatomic) IBOutlet UIButton *bFrom;
@property (weak, nonatomic) IBOutlet UIButton *bTo;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation CUExtratoViewController{
    NSDate* from;
    NSDate* to;
    NSArray* transactions;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    from = nil;
    to = nil;
    self.tableView.hidden = YES;
    [CUHistoryManager sharedManager].delegate = self;
    [CUWalletManager sharedManager].delegate = self;
    self.bTo.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.bFrom.imageView.contentMode = UIViewContentModeScaleAspectFit;
    to = [NSDate new];
    from = [to dateByAddingTimeInterval:-PRELOAD_DAYS_HISTORY*24*60*60];
    [self.bFrom setTitle:[TSUtils convertDate:from toStringWithFormat:@"dd/MM/yyyy"] forState:UIControlStateNormal];
    [self.bTo setTitle:[TSUtils convertDate:to toStringWithFormat:@"dd/MM/yyyy"] forState:UIControlStateNormal];
    [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_EXTRATO_IN_PROGRESS", nil)];
    [[CUHistoryManager sharedManager] performGetHistoryInRangeFrom:from to:to];
}

-(void) viewWillAppear:(BOOL)animated{
    [self showWallet:YES];
    [self updateWallet];
}

-(void) showWallet:(BOOL) show{
    if (show)
        [self.aivUpdateWallet stopAnimating];
    else
        [self.aivUpdateWallet startAnimating];
    self.bUpdateWallet.enabled = show;
    self.lSaldo.enabled = show;
}

-(void) updateWallet{
    self.lSaldo.text = [self stringWithCurrency:[CUWalletManager sharedManager].wallet.ballance];
}

- (IBAction)updateClick:(id)sender {
    [self showWallet:NO];
    [[CUWalletManager sharedManager] performUpdateWallet];
}

-(void) onWalletUpdateResult:(BOOL)success withSelectedWallet:(CUWallet *)wallet orError:(TSError *)error{
    [self showWallet:YES];
    if (success){
        [TSNotifier notify:NSLocalizedString(@"CUN_WALLET_UPDATED", nil) onView:self.view forTimeInterval:2];
        [self updateWallet];
    }
}

- (IBAction)fromClick:(id)sender {
    if (!from) from = [NSDate new];
    [ActionSheetDatePicker showPickerWithTitle:NSLocalizedString(@"PICKER_TITLE_SELECT_START_DATE", @"select start date") datePickerMode:UIDatePickerModeDate selectedDate:from doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        from = selectedDate;
        [self.bFrom setTitle:[TSUtils convertDate:from toStringWithFormat:@"dd/MM/yyyy"] forState:UIControlStateNormal];
    } cancelBlock:nil origin:self.view];
}

- (IBAction)toClick:(id)sender {
    if (!to) to = [NSDate new];
    [ActionSheetDatePicker showPickerWithTitle:NSLocalizedString(@"PICKER_TITLE_SELECT_END_DATE", @"select end date") datePickerMode:UIDatePickerModeDate selectedDate:to doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        to = selectedDate;
        [self.bTo setTitle:[TSUtils convertDate:to toStringWithFormat:@"dd/MM/yyyy"] forState:UIControlStateNormal];
    } cancelBlock:nil origin:self.view];
}
- (IBAction)enterClick:(id)sender {
    if (from == nil || to == nil){
        [TSNotifier notify:NSLocalizedString(@"CUN_ERROR_DATE_NOT_SET", @"One of the dates isn't set") onView:self.view];
    }
    else if ([from compare:to] == NSOrderedDescending){
        [TSNotifier notify:NSLocalizedString(@"CUN_ERROR_INCORRECT_DATES", @"Start later than end") onView:self.view];
        return;
    }
    else{
        [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_EXTRATO_IN_PROGRESS", nil)];
        [[CUHistoryManager sharedManager] performGetHistoryInRangeFrom:from to:to];
    }
}

-(void) onGetHistoryResult:(BOOL)success withTransactions:(NSArray *)_transactions orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    self.tableView.hidden = NO;
    if (success){
        transactions = _transactions;
        [self.tableView reloadData];
    }
    else{
        [TSNotifier alertError:error];
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return transactions.count;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CUTransactionCell height];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"CUTransactionCell";
    
    CUTransactionCell* cell = (CUTransactionCell*)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [cell setTransaction:[transactions objectAtIndex:indexPath.row]];
    return cell;
}


@end
