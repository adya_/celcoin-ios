//
//  CUAtivarTrocoViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/2/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUAtivarTrocoViewController.h"
#import "CUWallet.h"
#import "CUWalletManager.h"
#import "ZBarSDK.h"
#import "TSNotifier.h"
#import "CUStorage.h"

@interface CUAtivarTrocoViewController () <ZBarReaderDelegate, CUWalletCallback>
@property (weak, nonatomic) IBOutlet UILabel *lSaldo;
@property (weak, nonatomic) IBOutlet UIButton *bUpdateWallet;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivUpdateWallet;
@property (weak, nonatomic) IBOutlet UITextField *tfCode;

@end

@implementation CUAtivarTrocoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated{
    [self showWallet:YES];
    [self updateWallet];
    [CUWalletManager sharedManager].delegate = self;
}
- (IBAction)openScan:(id)sender {
    [self dismissKeyboard];
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    
    [scanner setSymbology:ZBAR_NONE config:ZBAR_CFG_ENABLE to:0];
    [scanner setSymbology:ZBAR_QRCODE config:ZBAR_CFG_ENABLE to:1];
    
    [self presentViewController:reader animated:YES completion:nil];
}

- (IBAction)continueClick:(id)sender {
    [self dismissKeyboard];
    NSString* code = self.tfCode.text;
    
    if (code && code.length > 0){
        [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_COUPON_ACTIVATION_IN_PROGRESS", nil)];
        [[CUWalletManager sharedManager] performRedeemCouponWithCode:code];
    }
    else
        [TSNotifier notify:NSLocalizedString(@"CUN_INVALID_CODE", nil) onView:self.view];
        
}

-(void) showWallet:(BOOL) show{
    if (show)
        [self.aivUpdateWallet stopAnimating];
    else
        [self.aivUpdateWallet startAnimating];
    self.bUpdateWallet.enabled = show;
    self.lSaldo.enabled = show;
}

-(void) updateWallet{
    self.lSaldo.text = [self stringWithCurrency:[CUWalletManager sharedManager].wallet.ballance];
}

- (IBAction)updateClick:(id)sender {
    [self showWallet:NO];
    [[CUWalletManager sharedManager] performUpdateWallet];
}

-(void) onWalletUpdateResult:(BOOL)success withSelectedWallet:(CUWallet *)wallet orError:(TSError *)error{
    [self showWallet:YES];
    if (success){
        [TSNotifier notify:NSLocalizedString(@"CUN_WALLET_UPDATED", nil) onView:self.view forTimeInterval:2];
        [self updateWallet];
    }
}


-(void) onWalletTransactionResult:(BOOL)success withProofMessage:(NSString *)message orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [[TSStorage sharedStorage] putValue:message forKey:STORAGE_VALUE_SUCCESS_MESSAGE];
        [[TSStorage sharedStorage] putValue:NSLocalizedString(@"CUN_MESSAGE_SUCCESS_ATIVAR", @"ativar success") forKey:STORAGE_VALUE_SUCCESS_TITLE];
        [[CUWalletManager sharedManager] performUpdateWallet];
        [self showSuccessViewController];
    }
    else{
        [TSNotifier alertError:error];
    }
}

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    
    self.tfCode.text = symbol.data;
    [reader dismissViewControllerAnimated:YES completion:nil];
}

@end
