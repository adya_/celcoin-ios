//
//  CUFaleConoscoViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 6/23/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUFaleConoscoViewController.h"
#import "CUPerguntasFrequentesViewController.h"

@interface CUFaleConoscoViewController () <TTTAttributedLabelDelegate>
@end

@implementation CUFaleConoscoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lLink.text = NSLocalizedString(@"FALE_CONOSCO", @" SOME LINK");
    self.showMenuButton = NO;
    [self.lLink addLinkToURL:[NSURL URLWithString:@"page://FAQ"] withRange:[self.lLink.text rangeOfString:@"perguntas frequentes"]];
    self.lLink.delegate = self;
}

-(void) attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url{
    if ([[url scheme] hasPrefix:@"page"] && [[url host] hasPrefix:@"FAQ"]){
        CUPerguntasFrequentesViewController* faq  = [self.storyboard instantiateViewControllerWithIdentifier:@"CUFAQ"];
        [self showSafeViewController:faq sender:self];
    }

}

@end
