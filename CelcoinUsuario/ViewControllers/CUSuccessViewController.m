//
//  CUSuccessViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/23/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUSuccessViewController.h"
#import "CUStorage.h"
#import "CUWalletManager.h"
#import "CUWallet.h"
#import "UILabel+HTML.h"

@interface CUSuccessViewController () <CUWalletCallback>
@property (weak, nonatomic) IBOutlet UILabel *lSuccess;
@property (weak, nonatomic) IBOutlet UILabel *lMessage;
@property (weak, nonatomic) IBOutlet UIWebView *wvMessage;
@property (weak, nonatomic) IBOutlet UILabel *lSaldo;
@end

@implementation CUSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showBackButton = NO;
    self.showMenuButton = NO;
    [CUWalletManager sharedManager].delegate = self;
}

-(void) onWalletUpdateResult:(BOOL)success withSelectedWallet:(CUWallet *)wallet orError:(TSError *)error{
    self.lSaldo.text = [NSString stringWithFormat:@"R$ %.2f", [CUWalletManager sharedManager].wallet.ballance];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.lSaldo.text = [NSString stringWithFormat:@"R$ %.2f", [CUWalletManager sharedManager].wallet.ballance];
    self.lSuccess.text = [[TSStorage sharedStorage] getValueForKey:STORAGE_VALUE_SUCCESS_TITLE];
    [self.wvMessage loadHTMLString:[[TSStorage sharedStorage] getValueForKey:STORAGE_VALUE_SUCCESS_MESSAGE] baseURL:nil];
   // [self.lMessage setHtml:[[TSStorage sharedStorage] getValueForKey:STORAGE_VALUE_SUCCESS_MESSAGE]];
    NSLog(@"Success title: %@", self.lSuccess.text);
    NSLog(@"Success message: %@", self.lMessage.text);
    [[TSStorage sharedStorage] removeValueForKey:STORAGE_VALUE_SUCCESS_TITLE];
    [[TSStorage sharedStorage] removeValueForKey:STORAGE_VALUE_SUCCESS_MESSAGE];
    
}


- (IBAction)finishClick:(id)sender {
    [self.navigationController popToViewController:[[TSStorage sharedStorage] getValueForKey:STORAGE_REF_MENU_VIEW_CONTROLLER] animated:YES];
}

@end
