//
//  LoginViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 6/21/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CULoginViewController.h"
#import "CULoginManager.h"
#import "TSNotifier.h"
#import "CUTools.h"
#import "CUWalletManager.h"
#import "CUStorage.h"
#import "TSError.h"
#import "CUUser.h"
#import "TSUtils.h"

#define FORGOT_PASSWORD_URL @"https://www.celcoin.com.br/Usuario/ForgotPassword"

@interface CULoginViewController () <CULoginCallback, CUWalletCallback>

@end

@implementation CULoginViewController{
    int requestsPending;
    BOOL errorOccurred;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tfLogin setLeftViewMode:UITextFieldViewModeAlways];
    UIImageView* img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 34, 24)];
    img.contentMode = UIViewContentModeScaleAspectFit;
    img.image = [UIImage imageNamed:@"login_logo_usuario"];
    self.tfLogin.leftView = img;
    
    [self.tfPassword setLeftViewMode:UITextFieldViewModeAlways];
    img = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 34, 24)];
    img.contentMode = UIViewContentModeScaleAspectFit;
    img.image = [UIImage imageNamed:@"login_logo_senha"];
    self.tfPassword.leftView = img;
    self.showMenuButton = NO;
    }


-(void) viewDidAppear:(BOOL)animated{
    [CULoginManager sharedManager].delegate = self;
    [CUWalletManager sharedManager].delegate = self;
    requestsPending = 0;
    errorOccurred = NO;
}

-(void) onLoginResult:(BOOL)success withUser:(CUUser *)user orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [self loadUser];
    }
    else {
        [TSNotifier alertError:error];
    }
}

-(void) loadUser{
    [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_LOADING_USER", @"Loading wallet")];
    [[CUWalletManager sharedManager] performUpdateWallet];
    [[CUWalletManager sharedManager] performUpdateBankReference];
    [[CUWalletManager sharedManager] performGetBanksList];
    [[CULoginManager sharedManager] performUpdateAccountInfo];
    requestsPending = 4;

}

-(void) onRegistrationInfoResult:(BOOL)success withInfo:(CUUserInfo *)info orError:(id)error{
    if (errorOccurred){
        requestsPending = 0;
        errorOccurred = NO;
        [TSNotifier hideProgressOnView:self.view];
        [self raiseError];
    }
    else if (success){
        requestsPending--;
        if (requestsPending == 0){
            [TSNotifier hideProgressOnView:self.view];
            [self enter];
        }
    }
    else{
        if (requestsPending == 1){
            [TSNotifier hideProgressOnView:self.view];
            [self raiseError];
        }
        else
            errorOccurred = YES;
    }
}

-(void) onWalletUpdateResult:(BOOL)success withSelectedWallet:(CUWallet *)wallet orError:(TSError *)error{
    
    if (errorOccurred){
        requestsPending = 0;
        errorOccurred = NO;
        [TSNotifier hideProgressOnView:self.view];
        [self raiseError];
    }
    else if (success){
        requestsPending--;
        if (requestsPending == 0){
            [TSNotifier hideProgressOnView:self.view];
            [self enter];
        }
    }
    else{
        if (requestsPending == 1){
            [TSNotifier hideProgressOnView:self.view];
            [self raiseError];
        }
        else
            errorOccurred = YES;
    }
    
    
}

-(void) onWalletBankReferenceUpdateResult:(BOOL)success withBankReference:(CUBankInfo *)bankReference orError:(TSError *)error{
    if (errorOccurred){
        requestsPending = 0;
        errorOccurred = NO;
        [TSNotifier hideProgressOnView:self.view];
        [self raiseError];
    }
    else if (success){
        requestsPending--;
        if (requestsPending == 0){
            [TSNotifier hideProgressOnView:self.view];
            [self enter];
        }
    }
    else{
        if (requestsPending == 1){
            [TSNotifier hideProgressOnView:self.view];
            [self raiseError];
        }
        else
            errorOccurred = YES;
    }
}

-(void) onWalletGetBanksListResult:(BOOL)success withList:(NSArray *)banksList orError:(TSError *)error{
    if (errorOccurred){
        requestsPending = 0;
        errorOccurred = NO;
        [TSNotifier hideProgressOnView:self.view];
        [self raiseError];
    }
    else if (success){
        [[TSStorage sharedStorage] putValue:banksList forKey:STORAGE_ARRAY_BANKS_LIST];
        requestsPending--;
        if (requestsPending == 0){
            [TSNotifier hideProgressOnView:self.view];
            [self enter];
        }
    }
    else{
        if (requestsPending == 1){
            [TSNotifier hideProgressOnView:self.view];
            [self raiseError];
        }
        else
            errorOccurred = YES;
    }
}

-(void) raiseError{
    [TSNotifier alert:NSLocalizedString(@"CUN_ERROR_LOADING_USER", nil) acceptButton:NSLocalizedString(@"CUN_TRY_AGAIN", nil) acceptBlock:^{
        [self loadUser];
    } cancelButton:@"OK" cancelBlock:^{
        [[CULoginManager  sharedManager] logout];
    }];
}

- (IBAction)enterClick:(id)sender {
    [self dismissKeyboard];
    NSString* email = trim(self.tfLogin.text);
    NSString* phone = [CUTools formattedNumber:email];
    NSString* pass = trim(self.tfPassword.text);
    BOOL isPhone = NO;
    if (![[CULoginManager sharedManager] isValidUsername:phone]){
        if (![[CULoginManager sharedManager] isValidUsername:email]){
            [TSNotifier alert:NSLocalizedString(@"CUN_INVALID_USERNAME", @"username")];
            return;
        }
        else{
            isPhone = NO;
        }
    }
    else{
        isPhone = YES;
    }
    if (![[CULoginManager sharedManager] isValidPassword:pass]){
        [TSNotifier alert:NSLocalizedString(@"CUN_INVALID_PASSWORD", @"password")];
    }
    else{
        [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_USER_LOGIN", @"login in..")];
        if (isPhone)
            [[CULoginManager sharedManager] performLoginWithUsername:phone andPassword:pass];
        else
            [[CULoginManager sharedManager] performLoginWithUsername:email andPassword:pass];
    }
}
- (IBAction)registerClick:(id)sender {
    [self dismissKeyboard];
    [self performSegueWithIdentifier:@"segLoginCadastro" sender:self];
}

- (IBAction)forgotPasswordClick:(id)sender {
    [[TSStorage sharedStorage] putValue:FORGOT_PASSWORD_URL forKey:STORAGE_VALUE_WEB_URL];
    [[TSStorage sharedStorage] putValue:NSLocalizedString(@"CU_TITLE_FORGOT_PASSWORD", nil) forKey:STORAGE_VALUE_WEB_TITLE];
    [[TSStorage sharedStorage] putValue:@(NO) forKey:STORAGE_FLAG_WEB_MENU];
    [self performSegueWithIdentifier:@"segLoginWebView" sender:self];
}

-(void) enter{
    // Before going to main menu display first-time user message.
    CUUser* user = [CULoginManager sharedManager].user;
    if (user.isUserMessageAvailable){
        [TSNotifier alert:user.userMessage];
    }
    [self performSegueWithIdentifier:@"segLoginMenu" sender:self];
    self.tfPassword.text = @"";
}

@end
