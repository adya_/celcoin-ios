//
//  CUCadastroCompletarViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/26/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUCadastroCompletarViewController.h"
#import "CUStorage.h"
#import "CULoginManager.h"
#import "TSError.h"
#import "TSNotifier.h"
#import "ActionSheetStringPicker.h"
#import "CUUser.h"
#import "CUUserInfo.h"

@interface CUCadastroCompletarViewController () <CULoginCallback>
@property (weak, nonatomic) IBOutlet UIButton *bOperator;

@end

@implementation CUCadastroCompletarViewController{
    NSArray* operators;
    NSArray* operatorsNames;
    int selectedOperatorIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showMenuButton = NO;
    operators = STORAGE_CONST_ARRAY_OPERATORS_CODES;
    operatorsNames = STORAGE_CONST_ARRAY_OPERATORS_NAMES;
    [CULoginManager sharedManager].delegate = self;
}
- (IBAction)completeClick:(id)sender {
    
    if (selectedOperatorIndex == -1){
        [TSNotifier alert:NSLocalizedString(@"CUN_SELECT_OPERATOR", nil)];
        return;
    }
    
    [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_COMPLETE_REGISTRATION_IN_PROGRESS", nil)];
    [[CULoginManager sharedManager] performCompleteRegistrationWithOperatorId: [[operators objectAtIndex:selectedOperatorIndex] intValue]];
}

- (IBAction)operatorClick:(id)sender {
    [self dismissKeyboard];
    [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"CU_TITLE_SELECT_OPERATOR", @"Selector") rows:operatorsNames initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        selectedOperatorIndex = (int)selectedIndex;
        [self.bOperator setTitle:selectedValue forState:UIControlStateNormal];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        selectedOperatorIndex = -1;
        [self.bOperator setTitle:NSLocalizedString(@"CU_TITLE_SELECT_OPERATOR", @"Selector") forState:UIControlStateNormal];
    } origin:self.view];
}

- (IBAction)cancelClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) onRegistrationResult:(BOOL)success withError:(TSError *)error{
    
    if (success){
        [[CULoginManager sharedManager] performUpdateAccountInfo];
    }
    else{
        [TSNotifier hideProgressOnView:self.view];
        [TSNotifier alertError:error];
    }
}

-(void) onRegistrationInfoResult:(BOOL)success withInfo:(CUUserInfo *)info orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [[TSStorage sharedStorage] putValue:@(YES) forKey:STORAGE_FLAG_REG_COMPLETE];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        [TSNotifier alertError:error];
    }
}

@end
