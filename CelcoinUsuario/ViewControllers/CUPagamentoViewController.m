//
//  CUPagamentoViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/2/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUPagamentoViewController.h"
#import "CUWalletManager.h"
#import "CUWallet.h"
#import "ZBarSDK.h"
#import "TSUtils.h"
#import "TSNotifier.h"
#import "CUStorage.h"
#import "CUTools.h"
#import "REFormattedNumberField.h"

static NSString* const placeholder = @"Código de barras";

@interface CUPagamentoViewController () <ZBarReaderDelegate, CUWalletCallback, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lSaldo;
@property (weak, nonatomic) IBOutlet UIButton *bCode;
@property (weak, nonatomic) IBOutlet UITextView *tvCode;
@property (weak, nonatomic) IBOutlet UIButton *bUpdateWallet;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivUpdateWallet;

@end

@implementation CUPagamentoViewController{
    NSString* format;
    NSString* longFormat;
    NSString* code;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bCode.imageView.contentMode = UIViewContentModeScaleAspectFit;
    format = @"XXXXX.XXXXX XXXXX.XXXXXX XXXXX.XXXXXX X XXXXXXXXXXXXXX";
    longFormat = @"XXXXXXXXXXX-X XXXXXXXXXXX-X XXXXXXXXXXX-X XXXXXXXXXXX-X";
    
    NSLog(@"%@ : %@", @"846 7 0000001 43590024020 02405000243 84221010811", [[self decodeCode48:@"846 7 0000001 43590024020 02405000243 84221010811"] re_stringWithNumberFormat:longFormat]);
    code = @"";
    self.tvCode.delegate = self;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:placeholder]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = placeholder;
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}

-(void) viewWillAppear:(BOOL)animated{
    [self showWallet:YES];
    [self updateWallet];
    [CUWalletManager sharedManager].delegate = self;
}

- (IBAction)openScan:(id)sender {
    [self dismissKeyboard];
    [TSNotifier notify:NSLocalizedString(@"CUN_OPENNING_CAMERA", nil) onView:self.view forTimeInterval:TS_NOTIFICATION_TIME_SHORT];
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    reader.videoQuality = UIImagePickerControllerQualityTypeHigh;

    
    ZBarImageScanner *scanner = reader.scanner;
    
    [scanner setSymbology:ZBAR_NONE config:ZBAR_CFG_ENABLE to:0];
    [scanner setSymbology:ZBAR_I25 config:ZBAR_CFG_ENABLE to:1];
    
    [self presentViewController:reader animated:YES completion:nil];
}

-(void) showWallet:(BOOL) show{
    if (show)
        [self.aivUpdateWallet stopAnimating];
    else
        [self.aivUpdateWallet startAnimating];
    self.bUpdateWallet.enabled = show;
    self.lSaldo.enabled = show;
}

-(void) updateWallet{
    self.lSaldo.text = [self stringWithCurrency:[CUWalletManager sharedManager].wallet.ballance];
}

- (IBAction)updateClick:(id)sender {
    [self showWallet:NO];
    [[CUWalletManager sharedManager] performUpdateWallet];
}

-(void) onWalletUpdateResult:(BOOL)success withSelectedWallet:(CUWallet *)wallet orError:(TSError *)error{
    [self showWallet:YES];
    if (success){
        [TSNotifier notify:NSLocalizedString(@"CUN_WALLET_UPDATED", nil) onView:self.view forTimeInterval:2];
        [self updateWallet];
    }
}

- (IBAction)continueClick:(id)sender {
    [self dismissKeyboard];
    if (code && code.length > 0){
        [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_REVIEW_PAYMENT_IN_PROGRESS", nil)];
        [[CUWalletManager sharedManager] performReviewPaymentWithCode:code];
    }
    else
        [TSNotifier notify:NSLocalizedString(@"CUN_PAGAMENTO_INVALID_CODE", nil) onView:self.view];
}

-(void) onWalletPaymentReviewResult:(BOOL)success withReviewInfo:(CUPaymentReview *)review orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success && review){
        [[TSStorage sharedStorage] putValue:review forKey:STORAGE_VALUE_PAYMENT_REVIEW];
        [self performSegueWithIdentifier:@"segPagamentosContinue" sender:self];
    }
    else
        [TSNotifier alertError:error];
}

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{

    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        break;
    NSLog(@"Scaned data: %@", symbol.data);
    NSString* val = [CUTools extractNumber:symbol.data];
    NSLog(@"Exrtacted data: %@", val);
    
    if ([self is47Code:val])
        code = [self decodeCode47:val];
    else{
        code = [self decodeCode48:val];
        NSLog(@"48-digits code");
    }
NSLog(@"Parsed data: %@", code);
    self.tvCode.text = code;
    [reader dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    NSString* current = [CUTools extractNumber:textView.text];
    NSString* replace =[CUTools extractNumber:text];
    
    int totalLength = (int)(current.length - range.length + replace.length);
    return totalLength <= 48;
}

-(BOOL) is47Code:(NSString*)_code{
    _code = [CUTools extractNumber:_code];
    return ![_code hasPrefix:@"8"];
}

-(void) textViewDidChange:(UITextView *)textView{
    NSString* newNumbers = [CUTools extractNumber:textView.text];
    NSString* oldNumbers = [CUTools extractNumber:code];
    if (oldNumbers.length == newNumbers.length)
        textView.text = [textView.text substringToIndex:(textView.text.length - 1)];
    textView.text = [textView.text re_stringWithNumberFormat:([self is47Code:newNumbers] ? format : longFormat)];
    code = textView.text;
}

- (NSString*) decodeCode48:(NSString*) _code{
    NSString* linha = [CUTools extractNumber:_code];
    if (linha.length != 44) // 'A linha do Código de Barras está incompleta!'
        return nil;

    NSString* campo1 = [NSString stringWithFormat:@"%@%@%@",[linha substringToIndex:3],[linha substringWithRange:NSMakeRange(3, 1)], [linha substringWithRange:NSMakeRange(4, 7)]];

    NSString* campo2 = [NSString stringWithFormat:@"%@",[linha substringWithRange:NSMakeRange(11, 11)]];

    NSString* campo3 = [NSString stringWithFormat:@"%@",[linha substringWithRange:NSMakeRange(22, 11)]];

    NSString* campo4 = [NSString stringWithFormat:@"%@",[linha substringWithRange:NSMakeRange(33, 11)]]; // Digito verificador
   
    return [NSString stringWithFormat:@"%@-%d %@-%d %@-%d %@-%d", campo1, [self modulo10:campo1], campo2, [self modulo10:campo2], campo3, [self modulo10:campo3], campo4, [self modulo10:campo4]];
}

- (NSString*) decodeCode47:(NSString*) _code{
    NSString* linha = [CUTools extractNumber:_code];
    if (linha.length != 44) // 'A linha do Código de Barras está incompleta!'
        return nil;
    // [0,4] + [19,20] + [20,24]
    NSString* campo1 = [NSString stringWithFormat:@"%@%@.%@",[linha substringWithRange:NSMakeRange(0, 4)], [linha substringWithRange:NSMakeRange(19, 1)],[linha substringWithRange:NSMakeRange(20, 4)]];
    // [24,29] + [29,34]
    NSString* campo2 = [NSString stringWithFormat:@"%@.%@", [linha substringWithRange:NSMakeRange(24, 5)], [linha substringWithRange:NSMakeRange(29, 5)]];
    // [34,39] + [39,44]
    NSString* campo3 = [NSString stringWithFormat:@"%@.%@", [linha substringWithRange:NSMakeRange(34, 5)], [linha substringWithRange:NSMakeRange(39, 5)]];
    // [4,5]
    NSString* campo4 = [linha substringWithRange:NSMakeRange(4, 1)]; // Digito verificador
    // [5,19]
    NSString* campo5 = [linha substringWithRange:NSMakeRange(5, 14)]; // Vencimento + Valor
    if ([self modulo11Banco:[NSString stringWithFormat:@"%@%@",[linha substringWithRange:NSMakeRange(0, 4)], [linha substringFromIndex:5]]] != [campo4 intValue]){
        return  nil; //'Digito verificador '+campo4+', o correto é '+modulo11_banco(  linha.substr(0,4)+linha.substr(5,99)  )+'\nO sistema não altera automaticamente o dígito correto na quinta casa!'
    }
    return [NSString stringWithFormat:@"%@%d %@%d %@%d %@ %@", campo1, [self modulo10:campo1], campo2, [self modulo10:campo2],campo3, [self modulo10:campo3], campo4, campo5];
}

-(int) modulo10:(NSString*) _numero{
    NSString* numero = [CUTools extractNumber:_numero];
    int soma = 0;
    int peso = 2;
    int contador = (int)numero.length-1;
    while (contador >= 0) {
        int multiplicacao = [[numero substringWithRange:NSMakeRange(contador, 1)] intValue] * peso;
        if (multiplicacao >= 10){
            multiplicacao = 1 + multiplicacao - 10;
        }
        soma += multiplicacao;
        peso = (peso == 2 ? 1 : 2);
        --contador;
    }
    int digito = 10 - (soma % 10);
    if (digito == 10) digito = 0;
    
    return digito;
}

-(int) modulo11Banco:(NSString*) _numero{
    NSString* numero = [CUTools extractNumber:_numero];
    int soma = 0;
    int peso  = 2;
    int base  = 9;
    int contador = (int)numero.length - 1;
    
    for (int i=contador; i >= 0; i--) {
        soma += [[numero substringWithRange:NSMakeRange(i, 1)] intValue] * peso;
        peso = ((peso < base) ? peso + 1 : 2);
    }
    int digito = 11 - (soma % 11);
    if (digito >  9) digito = 0;
    /* Utilizar o dígito 1(um) sempre que o resultado do cálculo padrão for igual a 0(zero), 1(um) ou 10(dez). */
    if (digito == 0) digito = 1;
    return digito;

}

@end
