//
//  CUPerguntasFrequentesViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 6/23/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUPerguntasFrequentesViewController.h"
#import "TSNotifier.h"
#import "TSUtils.h"

@interface CUPerguntasFrequentesViewController () <UIWebViewDelegate>

@end

@implementation CUPerguntasFrequentesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   // self.showMenuButton = NO;
    [self loadPage];
    self.wvPerguntas.delegate = self;
}

-(void) loadPage{
    [self.wvPerguntas loadHTMLString:NSLocalizedString(@"PERGUNTAS_FREQUENTES", @"perguntas") baseURL:nil];
}

-(void) webViewDidStartLoad:(UIWebView *)webView{
    [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_LOADING", @"Loading")];
}

-(void) webViewDidFinishLoad:(UIWebView *)webView{
    [TSNotifier hideProgressOnView:self.view];
}

-(void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    [TSNotifier alertWithTitle:APP_NAME message:error.localizedDescription acceptButton:NSLocalizedString(@"CUN_TRY_AGAIN", nil) acceptBlock:^{
        [self loadPage];
    }];
}


@end
