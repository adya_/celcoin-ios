//
//  CUPerguntasFrequentesViewController.h
//  CelcoinUsuario
//
//  Created by Adya on 6/23/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CUBaseViewController.h"

@interface CUPerguntasFrequentesViewController : CUBaseViewController
@property (weak, nonatomic) IBOutlet UIWebView *wvPerguntas;

@end
