//
//  CUPagamentoCompleteViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/3/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUPagamentoCompleteViewController.h"
#import "TSCurrencyTextField.h"
#import "ActionSheetDatePicker.h"
#import "TSUtils.h"
#import "CUStorage.h"
#import "TSNotifier.h"
#import "CUWalletManager.h"
#import "CUPaymentReview.h"

@interface CUPagamentoCompleteViewController () <CUWalletCallback>
@property (weak, nonatomic) IBOutlet UITextView *tvCode;
@property (weak, nonatomic) IBOutlet UITextField *tfDescription;
@property (weak, nonatomic) IBOutlet TSCurrencyTextField *tfValue;
@property (weak, nonatomic) IBOutlet UIButton *bDate;

@end

@implementation CUPagamentoCompleteViewController{
    CUPaymentReview* paymentReview;
    NSDate* date;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showMenuButton = NO;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    paymentReview = [[TSStorage sharedStorage] popValueForKey:STORAGE_VALUE_PAYMENT_REVIEW];
    self.tvCode.text = paymentReview.code;
    self.bDate.imageView.contentMode = UIViewContentModeScaleAspectFit;
    date = (paymentReview.date ? [TSUtils convertString:paymentReview.date  toDateWithFormat:@"dd/MM/yyyy"] : [NSDate new]);
    [self.bDate setTitle:[TSUtils convertDate:date toStringWithFormat:@"dd/MM/yyyy"] forState:UIControlStateNormal];
    self.tfDescription.text = paymentReview.assignor;
    self.tfValue.amount = [NSNumber numberWithFloat:paymentReview.value];
    [CUWalletManager sharedManager].delegate = self;
}
- (IBAction)selectDateClick:(id)sender {
    if (!date) date = [NSDate new];
    [ActionSheetDatePicker showPickerWithTitle:NSLocalizedString(@"PICKER_TITLE_SELECT_DATE", @"select start date") datePickerMode:UIDatePickerModeDate selectedDate:date doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        date = selectedDate;
        [self.bDate setTitle:[TSUtils convertDate:date toStringWithFormat:@"dd/MM/yyyy"] forState:UIControlStateNormal];
    } cancelBlock:nil origin:self.view];

}

- (IBAction)completeClick:(id)sender {
    [self dismissKeyboard];
    NSString* code = self.tvCode.text;
    NSString* assignor = self.tfDescription.text;
    float value = [self.tfValue.amount floatValue];
    
    if (!date)
        [TSNotifier notify:NSLocalizedString(@"CUN_INVALID_DATE_FORMAT", nil) onView:self.view];
    else if (value == 0)
        [TSNotifier notify:NSLocalizedString(@"CUN_SET_UP_VALUE_SAQUE", nil) onView:self.view];
    else if (assignor.length == 0)
        [TSNotifier notify:NSLocalizedString(@"CUN_TYPE_DESCRIPTION", nil) onView:self.view];
    else{
        
        NSString* msg = [NSString stringWithFormat:NSLocalizedString(@"CUN_CONFIRM_PAGAMENTO_MESSAGE", nil), assignor, value];
        [TSNotifier alertWithTitle:NSLocalizedString(@"CUN_CONFIRM_PAGAMENTO", nil) message:msg withAlignment:NSTextAlignmentLeft acceptButton:NSLocalizedString(@"CUN_YES", nil) acceptBlock:^{
            [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_PAYMENT_IN_PROGRESS", nil)];
            [[CUWalletManager sharedManager] performPayment:value withCode:code andDate:[TSUtils convertDate:date toStringWithFormat:@"yyyyMMdd"]];
            
        } cancelButton:NSLocalizedString(@"CUN_NO", nil) cancelBlock:nil];
        
        
    }
    
}

-(void) onWalletTransactionResult:(BOOL)success withProofMessage:(NSString *)message orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        message = [NSString stringWithUTF8String:[message UTF8String]];
        [[TSStorage sharedStorage] putValue:message forKey:STORAGE_VALUE_SUCCESS_MESSAGE];
        [[TSStorage sharedStorage] putValue:NSLocalizedString(@"CUN_MESSAGE_SUCCESS_PAGAMENTO", @"Pagamento success") forKey:STORAGE_VALUE_SUCCESS_TITLE];
        [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_UPDATING_WALLET", @"updating wallet")];
        [[CUWalletManager sharedManager] performUpdateWallet];
    }
    else{
        [TSNotifier alertError:error];
    }
}

-(void) onWalletUpdateResult:(BOOL)success withSelectedWallet:(CUWallet *)wallet orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    [self showSuccessViewController];
}

@end
