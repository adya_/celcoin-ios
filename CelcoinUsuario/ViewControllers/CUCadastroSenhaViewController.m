//
//  CUCadastroSenhaViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/3/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUCadastroSenhaViewController.h"
#import "CUStorage.h"
#import "CULoginManager.h"
#import "TSNotifier.h"
#import "CUTools.h"
#import "CUUser.h"

@interface CUCadastroSenhaViewController () <CULoginCallback>

@end

@implementation CUCadastroSenhaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showMenuButton = NO;
    //self.showBackButton = NO;
    [CULoginManager sharedManager].delegate = self;
}
- (IBAction)okClick:(id)sender {
    [self dismissKeyboard];
    NSString* phone = [[TSStorage sharedStorage] getValueForKey:STORAGE_VALUE_PHONE];
    NSString* sms = [[TSStorage sharedStorage] getValueForKey:STORAGE_VALUE_SMS_PASSWORD];
    NSString* smsDate = [[TSStorage sharedStorage] getValueForKey:STORAGE_VALUE_SMS_DATE];
    NSString* password = self.tfPassword.text;
    NSString* confirmPassword = self.tfConfirmPassword.text;
    
    if (![[CULoginManager sharedManager] isValidPassword:password]){
        [TSNotifier alert:NSLocalizedString(@"CUN_INVALID_PASSWORD", @"invalid password")];
    }
    else if (![password isEqualToString:confirmPassword]){
        [TSNotifier alert:NSLocalizedString(@"CUN_PPASSWORD_MISMATCH", @"Passwords mismatch")];
    }
    else{
        [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_USER_REGISTERING", @"creating user")];
        [[CULoginManager sharedManager] performRegistrationWithPhone:phone password:password SMSPassword:sms andSMSDate:smsDate loginOnSuccess:YES];
    }
}

-(void) onRegistrationResult:(BOOL)success withError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        //clean storage and move back
        [[TSStorage sharedStorage] removeValueForKey:STORAGE_VALUE_SMS_PASSWORD];
        [[TSStorage sharedStorage] removeValueForKey:STORAGE_VALUE_PHONE];
        [[TSStorage sharedStorage] removeValueForKey:STORAGE_VALUE_SMS_DATE];
        [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_USER_LOGIN", @"login in..")];
    }
    else{
        [TSNotifier alertError:error];
    }
}

-(void) onLoginResult:(BOOL)success withUser:(CUUser *)user orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        // Before going to main menu display first-time user message.
        if (user.isUserMessageAvailable){
            [TSNotifier alert:user.userMessage];
        }

        [self performSegueWithIdentifier:@"segCadastroMenu" sender:self];
    }
    else{
        [TSNotifier alertError:error];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (IBAction)backClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
