//
//  CUMainMenuViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 6/22/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUMainMenuViewController.h"
#import "TSNotifier.h"
#import "CUWalletManager.h"
#import "CUWallet.h"
#import "CUStorage.h"
#import "CULoginManager.h"
#import "CUUser.h"
#import "CUUserInfo.h"

#define MAP_URL @"https://www.easymapmaker.com/map/c5bf5c58b220e57ecf42f7b4937899b3"

@interface CUMainMenuViewController () <CUWalletCallback>
@property (weak, nonatomic) IBOutlet UIButton *bUpdateWallet;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivUpdateWallet;
@property (weak, nonatomic) IBOutlet UILabel *lSaldo;
@end

@implementation CUMainMenuViewController{
    NSString* storedSegue;
}

typedef NS_ENUM(NSInteger, MenuItems) {
    MI_EXTRATO = 1,
    MI_ATIVAR_VALE,
    MI_LOJAS,
    MI_RECARGA_CELULAR,
    MI_PAGAMENTO_CONTAS,
    MI_RECARGA_JOGOS,
    MI_SAQUE,
    MI_TRANSFERENCIA,
    MI_DEPOSITO,
    MI_TOTAL
};

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showBackButton = NO;
    self.showMenuButton = YES;
    [[TSStorage sharedStorage] putValue:self forKey:STORAGE_REF_MENU_VIEW_CONTROLLER];
    [self addLeftButton];
    self.navigationItem.title = @"Celcoin";
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self showWallet:YES];
    [self updateWallet];
    [CUWalletManager sharedManager].delegate = self;
    
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    BOOL success = [[[TSStorage sharedStorage] popValueForKey:STORAGE_FLAG_REG_COMPLETE] boolValue];
    if (storedSegue && success){
        [self completeRegistrationWithDelayedSegue:storedSegue];
    }
}

-(void)addLeftButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"logo_fundo_vermelho"];
    UIImageView* logo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
    logo.image = buttonImage;
    logo.contentMode = UIViewContentModeScaleAspectFit;
    UIBarButtonItem *aBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:logo];
    [self.navigationItem setLeftBarButtonItem:aBarButtonItem];
}

-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self addButtons];
}

-(void) showWallet:(BOOL) show{
    if (show)
        [self.aivUpdateWallet stopAnimating];
    else
        [self.aivUpdateWallet startAnimating];
    self.bUpdateWallet.enabled = show;
    self.lSaldo.enabled = show;
}

-(void) updateWallet{
    self.lSaldo.text = [self stringWithCurrency:[CUWalletManager sharedManager].wallet.ballance];
}

- (IBAction)updateClick:(id)sender {
    [self showWallet:NO];
    [[CUWalletManager sharedManager] performUpdateWallet];
}

-(void) onWalletUpdateResult:(BOOL)success withSelectedWallet:(CUWallet *)wallet orError:(TSError *)error{
    [self showWallet:YES];
    if (success){
        [TSNotifier notify:NSLocalizedString(@"CUN_WALLET_UPDATED", nil) onView:self.view forTimeInterval:2];
        [self updateWallet];
    }
}

-(void) addButtons{
    for (UIView* view in self.view.subviews) {
        if (view.tag == 0) continue;
        UIButton* button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
        button.tag = view.tag * 10;
        [button addTarget:self action:@selector(itemClicked:) forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:self action:@selector(itemDown:) forControlEvents:UIControlEventTouchDown];
        [view addSubview:button];
                                      
    }
}

-(UIImage*) getImageForViewWithTag:(NSInteger)tag highlighted:(BOOL) highlighted{
    NSString* imageName = nil;
    switch (tag) {
        case MI_EXTRATO: {
            imageName = @"btn_funcao_extrato";
            break;
        }
        case MI_ATIVAR_VALE: {
            imageName = @"btn_funcao_ativar_troco";
            break;
        }
        case MI_LOJAS: {
            imageName = @"btn_funcao_mapa";
            break;
        }
        case MI_RECARGA_CELULAR: {
            imageName = @"btn_funcao_recarga";
            break;
        }
        case MI_PAGAMENTO_CONTAS: {
            imageName = @"btn_funcao_pagto_contas";
            break;
        }
        case MI_RECARGA_JOGOS: {
            imageName = @"btn_funcao_jogos";
            break;
        }
        case MI_SAQUE: {
            imageName = @"btn_funcao_saque";
            break;
        }
        case MI_TRANSFERENCIA: {
            imageName = @"btn_funcao_transferencia";
            break;
        }
        case MI_DEPOSITO: {
            imageName = @"btn_funcao_deposito";
            break;
        }
        default: {
            return nil;
        }
    }
    
    if (highlighted)
        imageName = [NSString stringWithFormat:@"%@_pressed", imageName];
    return [UIImage imageNamed:imageName];
}

-(void) actionForTag:(NSInteger)tag{
    switch (tag) {
        case MI_EXTRATO: {
            [self performSegueWithIdentifier:@"segMenuExtrato" sender:self];
            break;
        }
        case MI_ATIVAR_VALE: {
            [self completeRegistrationWithDelayedSegue:@"segMenuAtivarTroco"];
            break;
        }
        case MI_LOJAS: {
            [[TSStorage sharedStorage] putValue:MAP_URL forKey:STORAGE_VALUE_WEB_URL];
            [[TSStorage sharedStorage] putValue:NSLocalizedString(@"CU_TITLE_LOJAS", nil) forKey:STORAGE_VALUE_WEB_TITLE];
            [[TSStorage sharedStorage] putValue:@(YES) forKey:STORAGE_FLAG_WEB_MENU];
            [self performSegueWithIdentifier:@"segMenuWebView" sender:self];
            break;
        }
        case MI_RECARGA_CELULAR: {
            [self completeRegistrationWithDelayedSegue:@"segMenuRecargaCelular"];
            break;
        }
        case MI_PAGAMENTO_CONTAS: {
            [self completeRegistrationWithDelayedSegue:@"segMenuPagamento"];
            break;
        }
        case MI_RECARGA_JOGOS: {
            [self completeRegistrationWithDelayedSegue:@"segMenuJogos"];
            break;
        }
        case MI_SAQUE: {
            [self completeRegistrationWithDelayedSegue:@"segMenuSaque"];
            break;
        }
        case MI_TRANSFERENCIA: {
            [self completeRegistrationWithDelayedSegue:@"segMenuTransferencia"];
            break;
        }
        case MI_DEPOSITO: {
            [self performSegueWithIdentifier:@"segMenuDeposito" sender:self];
            break;
        }
        default:
        case MI_TOTAL: {
            break;
        }
    }
}

-(IBAction)itemDown:(id)sender{
    UIView* target = (UIView*)sender;
    NSInteger viewTag = target.tag/10;
    UIImageView* img = [[self.view viewWithTag:viewTag].subviews objectAtIndex:0];
    img.image = [self getImageForViewWithTag:viewTag highlighted:YES];
}

- (IBAction)itemClicked:(id)sender {
    UIView* target = (UIView*)sender;
    NSInteger viewTag = target.tag/10;
    UIImageView* img = [[self.view viewWithTag:viewTag].subviews objectAtIndex:0];
    img.image = [self getImageForViewWithTag:viewTag highlighted:NO];
    [self actionForTag:viewTag];
}

-(void)  completeRegistrationWithDelayedSegue:(NSString*) delayedSegue{
    CUUserInfo* info = [CULoginManager sharedManager].user.info;
    if(!info || info.operatorId <= 0){
        storedSegue = delayedSegue;
        [self performSegueWithIdentifier:@"segMenuCompleteRegistrationOperator" sender:self];
    }
    else if (!info || !info.name || info.name.length == 0 || !info.cpf || info.cpf.length == 0){
        storedSegue = delayedSegue;
        [self performSegueWithIdentifier:@"segMenuCompleteRegistrationName" sender:self];
    }
    else{
        storedSegue = nil;
        [self performSegueWithIdentifier:delayedSegue sender:self];
    }
}

@end
