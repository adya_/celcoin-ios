//
//  CUSaqueViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/2/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUSaqueViewController.h"
#import "CUWalletManager.h"
#import "CUWallet.h"
#import "TSNotifier.h"
#import "CUBankInfo.h"
#import "TSCurrencyTextField.h"
#import "CUStorage.h"

@interface CUSaqueViewController () <CUWalletCallback>
@property (weak, nonatomic) IBOutlet UILabel *lSaldo;
@property (weak, nonatomic) IBOutlet UIButton *bUpdateWallet;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivUpdateWallet;
@property (weak, nonatomic) IBOutlet TSCurrencyTextField *tfValue;
@property (weak, nonatomic) IBOutlet UIButton *bBankReference;

@end

@implementation CUSaqueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bBankReference.titleLabel.numberOfLines = 1;
    self.bBankReference.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.bBankReference.titleLabel.lineBreakMode = NSLineBreakByClipping;}

-(void) viewWillAppear:(BOOL)animated{
    [self showWallet:YES];
    [self updateWallet];
    [CUWalletManager sharedManager].delegate = self;
    if ([CUWalletManager sharedManager].bankReference){
        [self.bBankReference setTitle:[CUWalletManager sharedManager].bankReference.bankDescription forState:UIControlStateNormal];
    }
}

-(void) showWallet:(BOOL) show{
    if (show)
        [self.aivUpdateWallet stopAnimating];
    else
        [self.aivUpdateWallet startAnimating];
    self.bUpdateWallet.enabled = show;
    self.lSaldo.enabled = show;
}

-(void) updateWallet{
    self.lSaldo.text = [self stringWithCurrency:[CUWalletManager sharedManager].wallet.ballance];
}

- (IBAction)updateClick:(id)sender {
    [self showWallet:NO];
    [[CUWalletManager sharedManager] performUpdateWallet];
}

-(void) onWalletUpdateResult:(BOOL)success withSelectedWallet:(CUWallet *)wallet orError:(TSError *)error{
    [self showWallet:YES];
    if (success){
        [TSNotifier notify:NSLocalizedString(@"CUN_WALLET_UPDATED", nil) onView:self.view forTimeInterval:2];
        [self updateWallet];
    }
}

- (IBAction)editBankRefClick:(id)sender {
    [self dismissKeyboard];
    [self performSegueWithIdentifier:@"segSaqueNext" sender:self];
}

- (IBAction)confirmClick:(id)sender {
[self dismissKeyboard];
    float value = [self.tfValue.amount floatValue];
    CUBankInfo* bankRef = [CUWalletManager sharedManager].bankReference;
    if (!bankRef){
        [TSNotifier notify:NSLocalizedString(@"CUN_SET_UP_BANK_ACCOUNT_SAQUE", nil) onView:self.view];
    }
    else if (value == 0){
        [TSNotifier notify:NSLocalizedString(@"CUN_SET_UP_VALUE_SAQUE", nil) onView:self.view];
    }
    else{
        NSString* msg = [NSString stringWithFormat:NSLocalizedString(@"CUN_CONFIRM_SAQUE_MESSAGE", nil), bankRef.bankID, bankRef.agency, bankRef.account, bankRef.bankAccountTypeName, bankRef.verificationDigit, value];
        
        [TSNotifier alertWithTitle:NSLocalizedString(@"CUN_CONFIRM_SAQUE", nil) message:msg withAlignment:NSTextAlignmentLeft acceptButton:NSLocalizedString(@"CUN_YES", nil) acceptBlock:^{
            [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_SAQUE_IN_PROGRESS", @"Saque loading")];
            [[CUWalletManager sharedManager] performWithdrawal:value];
            
        } cancelButton:NSLocalizedString(@"CUN_NO", nil) cancelBlock:nil];
    }
    
}

-(void) onWalletTransactionResult:(BOOL)success withProofMessage:(NSString *)message orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [[TSStorage sharedStorage] putValue:message forKey:STORAGE_VALUE_SUCCESS_MESSAGE];
        [[TSStorage sharedStorage] putValue:NSLocalizedString(@"CUN_MESSAGE_SUCCESS_SAQUE", @"Saque success") forKey:STORAGE_VALUE_SUCCESS_TITLE];
        [[CUWalletManager sharedManager] performUpdateWallet];
        [self showSuccessViewController];
    }
    else{
        [TSNotifier alertError:error];
    }
}

@end
