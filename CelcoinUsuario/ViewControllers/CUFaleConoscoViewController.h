//
//  CUFaleConoscoViewController.h
//  CelcoinUsuario
//
//  Created by Adya on 6/23/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTTAttributedLabel.h"
#import "CUBaseViewController.h"

@interface CUFaleConoscoViewController : CUBaseViewController
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *lLink;

@end
