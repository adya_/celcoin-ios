//
//  CUDepositoViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 6/23/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUDepositoViewController.h"
#import "UILabel+HTML.h"
#import <QuartzCore/QuartzCore.h>
#import "TSUtils.h"
#import "TSNotifier.h"

@interface CUDepositoViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lDepositoDescr;

@property (weak, nonatomic) IBOutlet UILabel *lDepositoDetails;
@property (weak, nonatomic) IBOutlet UIScrollView *vContainer;
@property (weak, nonatomic) IBOutlet UIView *vButtonDeposito;
@property (weak, nonatomic) IBOutlet UIView *vButtonBoleto;
@end

@implementation CUDepositoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.lDepositoDescr setHtml:NSLocalizedString(@"DEPOSITO_DESCRIPTION", @"description")];
    self.lDepositoDetails.text = @"";
    
    [self styleView:self.vContainer];
    [self styleView:self.vButtonBoleto];
    [self styleView:self.vButtonDeposito];
    
    [self setView:self.vButtonDeposito selected:NO];
    [self setView:self.vButtonBoleto selected:NO];
    }

-(void) styleView:(UIView*) view{
    view.layer.borderWidth = 2;
    view.layer.borderColor = [UIColor grayColor].CGColor;
    view.layer.cornerRadius = 5;
    view.layer.masksToBounds = YES;
}

-(void) setView:(UIView*) view selected:(BOOL) selected{
    view.backgroundColor = (selected?colorRGB(240, 220, 220):colorRGB(240, 240, 240));
}

-(void) highlightView:(UIView*) view{
    view.backgroundColor = colorRGB(240, 230, 230);
}

- (IBAction)bBancoDown:(id)sender {
    [self highlightView:self.vButtonDeposito];
}
- (IBAction)bBoletoDown:(id)sender {
    [self highlightView:self.vButtonBoleto];
}


- (IBAction)bBancoClick:(id)sender {
    [self setDetails:NSLocalizedString(@"DEPOSITO_BANCO_DESCRIPTION", @"description")];
    [self setView:self.vButtonBoleto selected:NO];
    [self setView:self.vButtonDeposito selected:YES];
    
}
- (IBAction)bBoletoClick:(id)sender {
    [self setDetails:NSLocalizedString(@"DEPOSITO_BOLETO_DESCRIPTION", @"description")];
    [self setView:self.vButtonBoleto selected:YES];
    [self setView:self.vButtonDeposito selected:NO];
}

-(void) setDetails:(NSString*) details{
    [self.lDepositoDetails setHtml:details];
    self.vContainer.contentSize = CGSizeMake(self.vContainer.contentSize.width, self.lDepositoDetails.frame.size.height + self.lDepositoDescr.frame.size.height + self.vButtonBoleto.frame.size.height + 32); // spaces between components
}

@end
