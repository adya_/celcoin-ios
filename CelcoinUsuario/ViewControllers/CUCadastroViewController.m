//
//  CUCadastroViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/3/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUCadastroViewController.h"
#import "CUTools.h"
#import "CUStorage.h"
#import "SHSPhoneTextField.h"
#import "TSNotifier.h"

@interface CUCadastroViewController ()
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *tfPhone;

@end

@implementation CUCadastroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showMenuButton = NO;
    [self.tfPhone.formatter setDefaultOutputPattern:@"(##) #####-####"];
    [self.tfPhone.formatter addOutputPattern:@"(##) ####-####" forRegExp:@"^(\\d{2})?(\\d{4})(\\d{4})$"];
    //self.showBackButton = NO;
}
- (IBAction)backClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)okClick:(id)sender {
    [self dismissKeyboard];
    NSString* phone = [CUTools formattedNumber:self.tfPhone.text];
    [[TSStorage sharedStorage] putValue:phone forKey:STORAGE_VALUE_PHONE];
    if ([CUTools isValidPhone:phone])
        [self performSegueWithIdentifier:@"segCadastroNext" sender:self];
    else
        [TSNotifier notify:NSLocalizedString(@"CUN_INVALID_USERNAME", @"username") onView:self.view];
}

@end
