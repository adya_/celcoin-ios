//
//  CUCadastroSenhaSMSViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/3/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUCadastroSenhaSMSViewController.h"
#import "TSNotifier.h"
#import "CULoginManager.h"
#import "CUStorage.h"
#import "TSUtils.h"
#import "CUTools.h"

@interface CUCadastroSenhaSMSViewController () <CULoginCallback>

@end

@implementation CUCadastroSenhaSMSViewController{
    NSString* date;
    NSString* phone;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showMenuButton = NO;
    self.showBackButton = NO;
    phone = [CUTools formattedNumber:[[TSStorage sharedStorage] getValueForKey:STORAGE_VALUE_PHONE]];
    [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_SMS_SENDING", @"Sending SMS")];
    [CULoginManager sharedManager].delegate = self;
    [[CULoginManager sharedManager] performSMSConfirmationWithPhone:phone
     ];
}

-(void) onSMSConfirmationResult:(BOOL)success withDate:(NSString*)_date orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        date = _date;
        [TSNotifier alert:NSLocalizedString(@"CUN_SMS_SENT", @"SMS sent")];
    }
    else{
        [TSNotifier alertError:error];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void) onSMSVerificationResult:(BOOL)success orError:(TSError *)error{
    NSString* sms = trim(self.tfSMS.text);
    NSString* smsDate = date;
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [[TSStorage sharedStorage] putValue:sms forKey:STORAGE_VALUE_SMS_PASSWORD];
        [[TSStorage sharedStorage] putValue:smsDate forKey:STORAGE_VALUE_SMS_DATE];
        [self performSegueWithIdentifier:@"segCadastroSMSSenha" sender:self];
    }
    else
        [TSNotifier alertError:error];
        
}

- (IBAction)smsOkClick:(id)sender {
    [self dismissKeyboard];
    NSString* sms = trim(self.tfSMS.text);
    NSString* smsDate = date;
    
    if (![[CULoginManager sharedManager] isValidSMSPassword:sms]){
        [TSNotifier alert:NSLocalizedString(@"CUN_INVALID_SMS_CODE", @"sms invalid")];
    }
    else if (![[CULoginManager sharedManager] isValidSMSDate:smsDate]){
        [TSNotifier alert:NSLocalizedString(@"CUN_INVALID_SMS_DATE", @"date invalid")];
    }
    else{
        
        [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_VERIFING_SMS", nil)];
        [[CULoginManager sharedManager] performSMSVerificationWithSMS:sms andSMSDate:smsDate forPhoneNumber:phone];
        
            }
}
- (IBAction)backClick:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
