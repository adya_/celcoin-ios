//
//  CURecargaCelularViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/2/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CURecargaCelularViewController.h"
#import "CUWallet.h"
#import "CUWalletManager.h"
#import "CUStorage.h"
#import "ActionSheetStringPicker.h"
#import "TSUtils.h"
#import "CUTools.h"
#import "TSNotifier.h"
#import "CULoginManager.h"
#import "CUUser.h"
#import "CUUserInfo.h"
#import "SHSPhoneTextField.h"

@interface CURecargaCelularViewController () <UICollectionViewDelegate, UICollectionViewDataSource, CUWalletCallback>
@property (weak, nonatomic) IBOutlet UILabel *lSaldo;
@property (weak, nonatomic) IBOutlet UIButton *bUpdateWallet;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivUpdateWallet;
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *tfPhone;
@property (weak, nonatomic) IBOutlet UICollectionView *cvButtons;
@property (weak, nonatomic) IBOutlet UIButton *bOperator;

@end


@implementation CURecargaCelularViewController{
    NSArray* operators;
    NSArray* operatorsNames;
    NSInteger selectedOperatorIndex;
    NSString* requestedPhone;
    NSInteger requestedOperatorIndex;
    NSArray* values;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    operatorsNames = STORAGE_CONST_ARRAY_OPERATORS_NAMES;
    operators = STORAGE_CONST_ARRAY_OPERATORS_CODES;
    [CUWalletManager sharedManager].delegate = self;
    selectedOperatorIndex = -1;
    [self.tfPhone.formatter setDefaultOutputPattern:@"(##) #####-####"];
    [self.tfPhone.formatter addOutputPattern:@"(##) ####-####" forRegExp:@"^(\\d{2})?(\\d{4})(\\d{4})$"];
//    [self.tfPhone.formatter addOutputPattern:@"(###) ####-####" forRegExp:@"^(\\d{3})?(\\d{4})(\\d{4})$"];
//    [self.tfPhone.formatter addOutputPattern:@"(###) #####-####" forRegExp:@"^(\\d{3})?(\\d{5})(\\d{4})$"];
}

-(void) viewWillAppear:(BOOL)animated{
    [self showWallet:YES];
    [self updateWallet];
    CUUser* user = [CULoginManager sharedManager].user;
    [self.tfPhone setFormattedText:[CUTools formattedLocalNumber:user.phoneNumber]];
    selectedOperatorIndex = [operators indexOfObject:@(user.info.operatorId)];
    if (selectedOperatorIndex != NSNotFound && selectedOperatorIndex != -1){
        [self.bOperator setTitle:[operatorsNames objectAtIndex:selectedOperatorIndex] forState:UIControlStateNormal];
        [self getAvailableValues];
    }
    else // there is no stored index select first one
        selectedOperatorIndex = 0;
    

}

-(void) showWallet:(BOOL) show{
    if (show)
        [self.aivUpdateWallet stopAnimating];
    else
        [self.aivUpdateWallet startAnimating];
    self.bUpdateWallet.enabled = show;
    self.lSaldo.enabled = show;
}

-(void) updateWallet{
    self.lSaldo.text = [self stringWithCurrency:[CUWalletManager sharedManager].wallet.ballance];
}

- (IBAction)updateClick:(id)sender {
    [self showWallet:NO];
    [[CUWalletManager sharedManager] performUpdateWallet];
}

-(void) onWalletUpdateResult:(BOOL)success withSelectedWallet:(CUWallet *)wallet orError:(TSError *)error{
    [self showWallet:YES];
    if (success){
        [TSNotifier notify:NSLocalizedString(@"CUN_WALLET_UPDATED", nil) onView:self.view forTimeInterval:2];
        [self updateWallet];
    }
}

- (IBAction)operatorClick:(id)sender {
    [self dismissKeyboard];
    [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"CU_TITLE_SELECT_OPERATOR", @"Selector") rows:operatorsNames initialSelection:selectedOperatorIndex doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        selectedOperatorIndex = selectedIndex;
        [self.bOperator setTitle:selectedValue forState:UIControlStateNormal];
        NSString* phone = [CUTools formattedNumber:trim(self.tfPhone.text)];
        if (selectedOperatorIndex != -1 && [CUTools isValidPhone:phone])
             [self getAvailableValues];
        else if (values.count != 0){
            [TSNotifier notify:NSLocalizedString(@"CUN_RECARGA_ENTER_VALID_NUMBER", nil) onView:self.view];
            values = [NSArray new];
            [self.cvButtons reloadData];
        }
    } cancelBlock:nil origin:self.view];
}

- (IBAction)phoneNumberEntered:(id)sender {
    NSString* phone = [CUTools formattedNumber:trim(self.tfPhone.text)];
    if (selectedOperatorIndex != -1 && [CUTools isValidPhone:phone])
        [self getAvailableValues];
    else if (values.count != 0){
        [TSNotifier notify:NSLocalizedString(@"CUN_RECARGA_SELECT_OPERATOR", nil) onView:self.view];
        values = [NSArray new];
        [self.cvButtons reloadData];
    }
}


-(void) getAvailableValues{
    NSString* phone = [CUTools formattedNumber:trim(self.tfPhone.text)];
    int operatorCode = [[operators objectAtIndex:selectedOperatorIndex] intValue];
    
    if ([phone isEqualToString:requestedPhone] && (selectedOperatorIndex == requestedOperatorIndex))
        return; // nothing changed so no need to refresh buttons.
    
    if (selectedOperatorIndex == -1){
        [TSNotifier alert:NSLocalizedString(@"CUN_SELECT_OPERATOR", nil)];
    }
    else if (![CUTools isValidPhone:phone]){
        [TSNotifier alert:NSLocalizedString(@"CUN_INVALID_NUMBER", nil)];
    }
    else{
        [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_GET_RECARGA_VALUES_IN_PROGRESS", nil)];
        [[CUWalletManager sharedManager] performGetRechargeValuesForPhone:phone andOperator:operatorCode];
    }
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return values.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"cvCell";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:100];
    [titleLabel setText: [NSString stringWithFormat:@"R$ %@",[values objectAtIndex:indexPath.row]]];
    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    UIImageView* iv = (UIImageView*)[cell viewWithTag:200];
    iv.image = [UIImage imageNamed:@"btn_valor_recarga_pressed"];
}

-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    UIImageView* iv = (UIImageView*)[cell viewWithTag:200];
    iv.image = [UIImage imageNamed:@"btn_valor_recarga"];
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self dismissKeyboard];
    int value = [values[indexPath.row] intValue];
    NSString* phone = [CUTools formattedNumber:trim(self.tfPhone.text)];
    
    if (selectedOperatorIndex == -1){
        [TSNotifier notify:NSLocalizedString(@"CUN_SELECT_OPERATOR", nil) onView:self.view];
    }
    else if (value <= 0){
        [TSNotifier notify:NSLocalizedString(@"CUN_INVALID_VALUE", nil) onView:self.view];
    }
    else if (phone.length == 0){
        [TSNotifier notify:NSLocalizedString(@"CUN_ENTER_PHONE", nil) onView:self.view];
    }
    else if (![CUTools isValidPhone:phone]){
        [TSNotifier notify:NSLocalizedString(@"CUN_INVALID_NUMBER", nil) onView:self.view];
    }
    else{
        NSString* msg = [NSString stringWithFormat:NSLocalizedString(@"CUN_CONFIRM_RECARGA_MESSAGE", nil), phone, value, [operatorsNames objectAtIndex:selectedOperatorIndex]];
        
        int operatorCode = [[operators objectAtIndex:selectedOperatorIndex] intValue];
        [TSNotifier alertWithTitle:NSLocalizedString(@"CUN_CONFIRM_RECARGA", nil) message:msg withAlignment:NSTextAlignmentLeft acceptButton:NSLocalizedString(@"CUN_YES", nil) acceptBlock:^{
            [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_RECHARGE_IN_PROGRESS", @"Recarga celular loading")];
            [[CUWalletManager sharedManager] performRechargePhoneWithNumber:phone operator:operatorCode withValue:value];
        } cancelButton:NSLocalizedString(@"CUN_NO", nil) cancelBlock:nil];

    }
}

-(void) onWalletGetAvailableRechargeValuesResult:(BOOL)success withValues:(NSArray *)_values orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        values = _values;
        requestedPhone = [CUTools formattedNumber:trim(self.tfPhone.text)];
        requestedOperatorIndex = selectedOperatorIndex;
        [self.cvButtons reloadData];
    }
    else{
        [TSNotifier alertError:error acceptButton:NSLocalizedString(@"CUN_TRY_AGAIN", nil) acceptBlock:^{
            [self getAvailableValues];
        } cancelButton:@"OK" cancelBlock:^{
            selectedOperatorIndex = requestedOperatorIndex;
            self.tfPhone.text = requestedPhone;
            [self.bOperator setTitle:[operatorsNames objectAtIndex:selectedOperatorIndex] forState:UIControlStateNormal];
        }];
    }
}

-(void) onWalletTransactionResult:(BOOL)success withProofMessage:(NSString *)message orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [[TSStorage sharedStorage] putValue:message forKey:STORAGE_VALUE_SUCCESS_MESSAGE];
        [[TSStorage sharedStorage] putValue:NSLocalizedString(@"CUN_MESSAGE_SUCCESS_RECARGA", nil) forKey:STORAGE_VALUE_SUCCESS_TITLE];
        [[CUWalletManager sharedManager] performUpdateWallet];
        [self showSuccessViewController];
    }
    else{
        [TSNotifier alertError:error];
    }
}

@end
