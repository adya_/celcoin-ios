//
//  CUTransferenciaPhoneViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 8/14/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUTransferenciaPhoneViewController.h"
#import "CUWalletManager.h"
#import "CUWallet.h"
#import "TSCurrencyTextField.h"
#import "TSNotifier.h"
#import "SHSPhoneTextField.h"
#import "CUTools.h"
#import "TSContactsManager.h"
#import "CUStorage.h"
#import "CULoginManager.h"

@interface CUTransferenciaPhoneViewController () <CUWalletCallback>
@property (weak, nonatomic) IBOutlet UILabel *lSaldo;
@property (weak, nonatomic) IBOutlet UIButton *bUpdateWallet;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivUpdateWallet;
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *tfPhone;
@property (weak, nonatomic) IBOutlet TSCurrencyTextField *tfValue;

@end

@implementation CUTransferenciaPhoneViewController{
    TSContact* contact;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showMenuButton = YES;
    [self.tfPhone.formatter setDefaultOutputPattern:@" (##) #####-####"];
    [self.tfPhone.formatter addOutputPattern:@"(##) ####-####" forRegExp:@"^(\\d{2})?(\\d{4})(\\d{4})$"];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self showWallet:YES];
    [self updateWallet];
    [CUWalletManager sharedManager].delegate = self;
}

-(void) viewDidAppear:(BOOL)animated{
    contact = ((TSContact*)[[TSStorage sharedStorage] popValueForKey:STORAGE_VALUE_CONTACT_TRANSFER]);
    if (contact){
        [self.tfPhone setFormattedText:[CUTools formattedLocalNumber:((TSPhone*)[contact.auxPhones objectAtIndex:0]).number]];
    }
}

-(void) showWallet:(BOOL) show{
    if (show)
        [self.aivUpdateWallet stopAnimating];
    else
        [self.aivUpdateWallet startAnimating];
    self.bUpdateWallet.enabled = show;
    self.lSaldo.enabled = show;
}

-(void) updateWallet{
    self.lSaldo.text = [self stringWithCurrency:[CUWalletManager sharedManager].wallet.ballance];
}

- (IBAction)updateClick:(id)sender {
    [self showWallet:NO];
    [[CUWalletManager sharedManager] performUpdateWallet];
}

-(void) onWalletUpdateResult:(BOOL)success withSelectedWallet:(CUWallet *)wallet orError:(TSError *)error{
    [self showWallet:YES];
    if (success){
        [TSNotifier notify:NSLocalizedString(@"CUN_WALLET_UPDATED", nil) onView:self.view forTimeInterval:2];
        [self updateWallet];
    }
}

- (IBAction)completeClick:(id)sender {
    [self dismissKeyboard];
    float value = [self.tfValue.amount floatValue];
    NSString* phone = [CUTools formattedLocalNumber:self.tfPhone.text];
    NSString* name = [CUTools prettyNumber:phone];
    
    if (contact)
        name = contact.fullName;
    
    if (value == 0)
        [TSNotifier notify:NSLocalizedString(@"CUN_SET_UP_VALUE_TRANSFERENCIA", nil) onView:self.view];
    else if ([CUTools isValidPhone:phone]){
            phone = [CUTools formattedNumber:phone];
            NSLog(@"phone = %@", phone);
            NSString* msg = [NSString stringWithFormat:NSLocalizedString(@"CUN_CONFIRM_TRANSFERENCIA_MESSAGE", nil), name, phone, value];
            
            [TSNotifier alertWithTitle:NSLocalizedString(@"CUN_CONFIRM_TRANSFERENCIA", nil) message:msg withAlignment:NSTextAlignmentLeft acceptButton:NSLocalizedString(@"CUN_YES", nil) acceptBlock:^{
                [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_TRANSFERENCIA_IN_PROGRESS", nil)];
                [[CUWalletManager sharedManager] performTransfer:value toPhoneWithNumber:phone];
            } cancelButton:NSLocalizedString(@"CUN_NO", nil) cancelBlock:nil];
        }
}

-(void) onWalletTransactionResult:(BOOL)success withProofMessage:(NSString *)message orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        [[TSStorage sharedStorage] putValue:message forKey:STORAGE_VALUE_SUCCESS_MESSAGE];
        [[TSStorage sharedStorage] putValue:NSLocalizedString(@"CUN_MESSAGE_SUCCESS_SAQUE", @"Saque success") forKey:STORAGE_VALUE_SUCCESS_TITLE];
        [[CUWalletManager sharedManager] performUpdateWallet];
        [self showSuccessViewController];
    }
    else{
        [TSNotifier alertError:error];
    }
}





@end
