//
//  CULojasViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/2/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUWebViewController.h"
#import "TSNotifier.h"
#import "TSError.h"
#import "TSUtils.h"
#import "CUStorage.h"



@interface CUWebViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *wvMap;

@end

@implementation CUWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadPage];
    self.showMenuButton = [[[TSStorage sharedStorage] popValueForKey:STORAGE_FLAG_WEB_MENU] boolValue];
    self.title = [[TSStorage sharedStorage] popValueForKey:STORAGE_VALUE_WEB_TITLE];
}

-(void) loadPage{
    NSString* url = [[TSStorage sharedStorage] getValueForKey:STORAGE_VALUE_WEB_URL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [self.wvMap loadRequest:requestObj];
}

-(void) webViewDidStartLoad:(UIWebView *)webView{
    [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_LOADING", @"Loading")];
}

-(void) webViewDidFinishLoad:(UIWebView *)webView{
    [TSNotifier hideProgressOnView:self.view];
}

-(void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    [TSNotifier alertWithTitle:APP_NAME message:error.localizedDescription acceptButton:NSLocalizedString(@"CUN_TRY_AGAIN", nil) acceptBlock:^{
        [self loadPage];
    }];
}

@end
