//
//  CUTransferenciaViewController.m
//  CelcoinUsuario
//
//  Created by Adya on 7/2/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUTransferenciaViewController.h"
#import "TSContactsManager.h"
#import "CUContactCell.h"
#import "TSNotifier.h"
#import "TSUtils.h"
#import "TSStorage.h"
#import "CUStorage.h"
#import "CULoginManager.h"
#import "ActionSheetStringPicker.h"
#import "CUTools.h"

@interface CUTransferenciaViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, TSContactManagerProgressDelegate, TSContactManagerAccessDelegate, CULoginCallback>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lEmpty;

@end

@implementation CUTransferenciaViewController{
    NSArray* contacts;
    NSArray* searchResult;
    
    NSDictionary* celcoinUsers;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    TSContactsManager* manager = [TSContactsManager sharedManager];
    [CULoginManager sharedManager].delegate = self;
    manager.accessDelegate = self;
    manager.progressDelegate = self;
    if (![TSContactsManager sharedManager].isLoaded){
        [[TSContactsManager sharedManager] refresh];
    }
    else{
        contacts = [self sortContacts:[TSContactsManager sharedManager].contactsBook];
        if (!celcoinUsers || celcoinUsers.count == 0)
        [self checkCelcoinContacts];
        [self showContacts];
    }
}
- (IBAction)refreshClick:(id)sender {
    [[TSContactsManager sharedManager] refresh];
}

-(void) onRequestAccessResult:(BOOL)result wasDenied:(BOOL)wasDenied{
    if (!result){
        [self showContacts];
        if (wasDenied){
            float ios = SYSTEM_VERSION;
            BOOL autoOpenSettings = (ios >= 8.0);
            NSString* msg = [NSString stringWithFormat:NSLocalizedString(@"CUN_ACCESS_ADDRESS_BOOK", nil), APP_NAME];
            if (autoOpenSettings)
                msg = [NSString stringWithFormat:@"%@ %@", msg, NSLocalizedString(@"CUN_ACCESS_ADDRESS_BOOK_MANUALLY", nil)];
            NSString* accept = (!autoOpenSettings ? NSLocalizedString(@"CUN_BUTTON_ACCESS_ADDRESS_BOOK_MANUALLY", nil) : NSLocalizedString(@"CUN_BUTTON_ACCESS_ADDRESS_BOOK", nil));
            [TSNotifier alertWithTitle:NSLocalizedString(@"CUN_ACCESS_ADDRESS_BOOK_DENIED", nil) message:msg acceptButton:accept acceptBlock:^{
                if (autoOpenSettings)
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
            }
            cancelButton:NSLocalizedString(@"CUN_BUTTON_ACCESS_ADDRESS_BOOK_LATER", nil) cancelBlock:^{
                [self.navigationController popViewControllerAnimated:YES];
            }];
        }
    }
}

-(void) showContacts{
    self.tableView.hidden = (contacts.count == 0);
    self.lEmpty.hidden = (contacts.count != 0);
}

-(NSArray*) sortContacts:(NSArray*)_contacts{
    return [_contacts sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        TSContact* f = (TSContact*)obj1;
        TSContact* s = (TSContact*)obj2;
        return [f.fullName compare:s.fullName];
    }];
}

-(void) onReadingContactsHasStarted:(int)totalAmount{
    [TSNotifier showProgressOnView:self.view withMessage:NSLocalizedString(@"CUN_LOADING_CONTACTS", nil)];
}

-(void) onReadingContactsHasFinished:(NSArray *)_contacts{
    [TSNotifier hideProgressOnView:self.view];
    contacts = [self sortContacts:_contacts];
    [self showContacts];
    if (contacts.count != 0){
        [self checkCelcoinContacts];
        
    }
}

-(void) checkCelcoinContacts{
    [TSNotifier showProgressOnView:self.view withMessage:@"Checking user.."];
    [[CULoginManager sharedManager] performCheckCelcoinContacts:contacts];
}

-(void) onCelcoinContactCheckedResult:(BOOL)success withCelcoinContacts:(NSDictionary*) celcoinContacts orError:(TSError *)error{
    [TSNotifier hideProgressOnView:self.view];
    if (success){
        celcoinUsers = celcoinContacts;
        [self.tableView reloadData];
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.searchDisplayController.searchResultsTableView)
        return searchResult.count;
    else
        return contacts.count;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [CUContactCell height];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"CUContactCell";
    
    CUContactCell* cell = (CUContactCell*)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    TSContact* c = (tableView == self.searchDisplayController.searchResultsTableView ? [searchResult objectAtIndex:indexPath.row] : [contacts objectAtIndex:indexPath.row]);
    [cell setContact:c];
    cell.isCelcoin = ([celcoinUsers objectForKey:c.fullName]);
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TSContact* contact = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView)
            contact = [searchResult objectAtIndex:indexPath.row];
    else
        contact = [contacts objectAtIndex:indexPath.row];
    TSContact* celcoinContact = [celcoinUsers objectForKey:contact.fullName];
    if (celcoinContact)
        contact = celcoinContact;
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (contact.auxPhones.count > 1){
        NSArray* phones = [contact.auxPhones valueForKey:@"number"];
        NSMutableArray* plainPhones = [NSMutableArray new];
        for (NSString* phone in phones) {
            [plainPhones addObject:[CUTools prettyNumber:phone]];
        }
 
        [ActionSheetStringPicker showPickerWithTitle:@"Select phone" rows:plainPhones initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
            TSContact* c = [TSContact new];
            c.firstName = contact.firstName;
            c.lastName = contact.lastName;
            c.middleName = contact.middleName;
            TSPhone* phone = [TSPhone new];
            phone.number = [plainPhones objectAtIndex:selectedIndex];
            c.auxPhones = @[phone];
            [[TSStorage sharedStorage] putValue:c forKey:STORAGE_VALUE_CONTACT_TRANSFER];
            [self.navigationController popViewControllerAnimated:YES];
        } cancelBlock:nil origin:self.view];
    }
    else{
        [[TSStorage sharedStorage] putValue:contact forKey:STORAGE_VALUE_CONTACT_TRANSFER];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"fullName contains[cd] %@",
                                    searchText];
    
    searchResult = [contacts filteredArrayUsingPredicate:resultPredicate];
}

@end
