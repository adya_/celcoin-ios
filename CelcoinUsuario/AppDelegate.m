//
//  AppDelegate.m
//  CelcoinUsuario
//
//  Created by Adya on 6/21/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "AppDelegate.h"
#import "TestFairy.h"
#import "TSUtils.h"
#import "CUTools.h"
#import "AFNetworkActivityIndicatorManager.h"

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   
    // +5511980776232
    // 123123
    
//    NSLog(@"%@ --> %@", @"12345678", [CUTools prettyNumber:@"12345678"]);
//    NSLog(@"%@ --> %@", @"1112345678", [CUTools prettyNumber:@"1112345678"]);
//    NSLog(@"%@ --> %@", @"+5512345678", [CUTools prettyNumber:@"+5512345678"]);
//    NSLog(@"%@ --> %@", @"+551112345678", [CUTools prettyNumber:@"+551112345678"]);
//    
//    NSLog(@"%@ --> %@", @"123456789", [CUTools prettyNumber:@"123456789"]);
//    NSLog(@"%@ --> %@", @"11123456789", [CUTools prettyNumber:@"11123456789"]);
//    NSLog(@"%@ --> %@", @"+55123456789", [CUTools prettyNumber:@"+55123456789"]);
//    NSLog(@"%@ --> %@", @"+5511123456789", [CUTools prettyNumber:@"+5511123456789"]);
//    NSLog(@"%@ --> %@", @"031980776232", [CUTools prettyNumber:@"031980776232"]);
//    NSLog(@"%@ --> %@", @"31980776232", [CUTools prettyNumber:@"31980776232"]);
//    
//    NSLog(@"%@ --> %@", @"123456789", [CUTools formattedLocalNumber:@"123456789"]);
//    NSLog(@"%@ --> %@", @"11123456789", [CUTools formattedLocalNumber:@"11123456789"]);
//    NSLog(@"%@ --> %@", @"+55123456789", [CUTools formattedLocalNumber:@"+55123456789"]);
//    NSLog(@"%@ --> %@", @"+5511123456789", [CUTools formattedLocalNumber:@"+5511123456789"]);
//    NSLog(@"%@ --> %@", @"031980776232", [CUTools formattedLocalNumber:@"031980776232"]);
//    NSLog(@"%@ --> %@", @"31980776232", [CUTools formattedLocalNumber:@"31980776232"]);

    
//    NSLog(@"%@ --> %@", @"123456789", [CUTools formattedNumber:@"123456789"]);
//    NSLog(@"%@ --> %@", @"11123456789", [CUTools formattedNumber:@"11123456789"]);
//    NSLog(@"%@ --> %@", @"+55123456789", [CUTools formattedNumber:@"+55123456789"]);
//    NSLog(@"%@ --> %@", @"+5511123456789", [CUTools formattedNumber:@"+5511123456789"]);

    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    [TestFairy begin:@"8b0874a39eda7d685f5c86bfeab27646d9d4fa94"];
    [application setStatusBarHidden:NO];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
@end
