//
//  CUUserInfo.h
//  CelcoinUsuario
//
//  Created by Adya on 7/29/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"

@interface CUUserInfo : NSObject <TSJSONParselable>
//"CadastroId": 7,
//"DataNascimento": "19830907",
//"Documento": "11036382702   ",
//"Email": "daniel.arjuna@is2b.com.br",
//"MailMarketing": false,
//"Nome": "Daniel Arjuna da Costa",
//"OperadoraId": 2089,
//"RecargaAutomatica": false,
//"Sexo": "M",
//"Telefone": "+5511980776232"
@property (readonly) int registrationId;
@property (readonly) NSString* email;
@property (readonly) NSString* name;
@property (readonly) NSString* cpf;
@property (readonly) int operatorId;
@property (readonly) NSString* phoneNumber;
@end
