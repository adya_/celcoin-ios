//
//  CUUserInfo.m
//  CelcoinUsuario
//
//  Created by Adya on 7/29/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUUserInfo.h"
#import "TSUtils.h"

@implementation CUUserInfo

@synthesize registrationId;
@synthesize email;
@synthesize name;
@synthesize cpf;
@synthesize operatorId;
@synthesize phoneNumber;

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    registrationId = [nonNull([jsonObject objectForKey:@"CadastroId"]) intValue];
    email = nonNull([jsonObject objectForKey:@"Email"]);
    name = nonNull([jsonObject objectForKey:@"Nome"]);
    cpf = nonNull([jsonObject objectForKey:@"Documento"]);
    operatorId = [nonNull([jsonObject objectForKey:@"OperadoraId"]) intValue];
    phoneNumber = nonNull([jsonObject objectForKey:@"Telefone"]);
    return self;
}
@end
