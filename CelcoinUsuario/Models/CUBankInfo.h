//
//  CUBankInfo.h
//  CelcoinUsuario
//
//  Created by Adya on 7/8/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"

@interface CUBankInfo : NSObject <TSJSONParselable>

@property int agency;
@property int bankID;
@property int registrationID;
@property int account;
@property NSString* description;
@property NSString* bankDescription;
@property NSString* verificationDigit;
@property int bankReferenceID;
@property int bankAccountType;
@property (readonly) NSString* bankAccountTypeName;

-(id) initWithBankInfo:(CUBankInfo*) copy;
@end
