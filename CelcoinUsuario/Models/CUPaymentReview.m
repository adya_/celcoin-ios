//
//  CUPaymentReview.m
//  CelcoinUsuario
//
//  Created by Adya on 7/27/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUPaymentReview.h"
#import "TSUtils.h"

@implementation CUPaymentReview

@synthesize assignor;
@synthesize value;
@synthesize date;
@synthesize code;

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    if (!jsonObject || jsonObject.count == 0) return nil;
    assignor = nonNull([jsonObject valueForKey:@"cedente"]);
    value = [nonNull([jsonObject valueForKey:@"valorLido"]) floatValue];
    date = nonNull([jsonObject valueForKey:@"dataVencimento"]);
    return self;
}
@end
