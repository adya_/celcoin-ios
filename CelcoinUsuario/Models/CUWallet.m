//
//  CUWallet.m
//  CelcoinUsuario
//
//  Created by Adya on 7/17/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUWallet.h"
#import "TSUtils.h"

@implementation CUWallet

@synthesize ballance;
@synthesize currency;

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    if (!jsonObject || jsonObject.count == 0) return nil;
    ballance = [nonNull([jsonObject valueForKey:@"Saldo"]) doubleValue];
    currency = [nonNull([jsonObject valueForKey:@"Moeda"]) intValue];
    return self;
}

@end
