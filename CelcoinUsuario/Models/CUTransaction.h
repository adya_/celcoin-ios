//
//  CUTransaction.h
//  CelcoinUsuario
//
//  Created by Adya on 7/8/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"

@interface CUTransaction : NSObject <TSJSONParselable>

@property NSString* date;
@property NSString* description;
@property NSString* storeName;
@property float value;

@end
