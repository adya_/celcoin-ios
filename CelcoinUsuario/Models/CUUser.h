//
//  CUUser.h
//  CelcoinUsuario
//
//  Created by Adya on 7/8/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"

@class CUUserInfo;

@interface CUUser : NSObject <TSJSONParselable>

@property NSString* phoneNumber;
@property (readonly) NSString* accessToken;
@property (readonly) NSString* token;

@property CUUserInfo* info;
@property (readonly) NSString* userMessage;
@property (readonly) BOOL isUserMessageAvailable;

-(id) initWithPhone:(NSString*) phoneNumber;
-(id) initWithUser:(CUUser*) user;
@end
