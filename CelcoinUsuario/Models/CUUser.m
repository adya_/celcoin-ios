//
//  CUUser.m
//  CelcoinUsuario
//
//  Created by Adya on 7/8/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUUser.h"
#import "TSUtils.h"
#import "TSNotifier.h"

@implementation CUUser


@synthesize phoneNumber;
@synthesize accessToken;
@synthesize token;

@synthesize info;
@synthesize userMessage;
@synthesize isUserMessageAvailable;

-(id) init{
    self = [super init];
    userMessage = nil;
    isUserMessageAvailable = NO;
    return self;
}

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    if (!jsonObject || jsonObject.count == 0) return nil;
    NSArray* weirdArray = nonNull([jsonObject valueForKey:@"FirstAccessMsg"]);
    userMessage = (weirdArray && weirdArray.count != 0? [weirdArray objectAtIndex:0] : nil);
    userMessage = [userMessage stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"]; // fix server response with invalid breakline symbol.
    isUserMessageAvailable = [nonNull([jsonObject valueForKey:@"FirstAccess"]) boolValue];
//#warning testing
    //isUserMessageAvailable = YES;
    //userMessage = @"TESTED MESSAGE";
    accessToken = nonNull([jsonObject valueForKey:@"AccessToken"]);
    token = nonNull([jsonObject valueForKey:@"Token"]);
    return self;
}

-(id) initWithPhone:(NSString*) _phoneNumber
{
    self = [self init];
    phoneNumber = _phoneNumber;
    return self;
}

-(id) initWithUser:(CUUser*) user
{
    self = [self init];
    accessToken = user.accessToken;
    token = user.token;
    isUserMessageAvailable = user.isUserMessageAvailable;
    userMessage = user.userMessage;
    phoneNumber = user.phoneNumber;
    info = user.info;
    return self;
}

@end
