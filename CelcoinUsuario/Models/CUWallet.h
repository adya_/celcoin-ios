//
//  CUWallet.h
//  CelcoinUsuario
//
//  Created by Adya on 7/17/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"

@interface CUWallet : NSObject <TSJSONParselable>

@property double ballance;
@property int currency;

@end
