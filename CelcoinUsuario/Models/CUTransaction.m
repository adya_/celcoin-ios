//
//  CUTransaction.m
//  CelcoinUsuario
//
//  Created by Adya on 7/8/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUTransaction.h"
#import "TSJSONParselable.h"
#import "TSUtils.h"

@implementation CUTransaction

@synthesize date;
@synthesize description;
@synthesize storeName;
@synthesize value;

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    if (!jsonObject || jsonObject.count == 0) return nil;
    date = nonNull([jsonObject valueForKey:@"Data"]);
    description = nonNull([jsonObject valueForKey:@"Descricao"]);
    storeName = nonNull([jsonObject valueForKey:@"NomeLoja"]);
    value = [nonNull([jsonObject valueForKey:@"Valor"]) floatValue];
    return self;
}

@end
