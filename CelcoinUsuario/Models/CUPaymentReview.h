//
//  CUPaymentReview.h
//  CelcoinUsuario
//
//  Created by Adya on 7/27/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSJSONParselable.h"

@interface CUPaymentReview : NSObject <TSJSONParselable>
    @property NSString* assignor;
    @property float value;
    @property NSString* date;
    @property NSString* code;
@end
