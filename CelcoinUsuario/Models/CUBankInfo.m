//
//  CUBankInfo.m
//  CelcoinUsuario
//
//  Created by Adya on 7/8/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUBankInfo.h"
#import "TSUtils.h"

@implementation CUBankInfo


@synthesize agency;
@synthesize bankID;
@synthesize registrationID;
@synthesize account;
@synthesize description;
@synthesize bankDescription;
@synthesize verificationDigit;
@synthesize bankReferenceID;
@synthesize bankAccountType;

-(NSString*) bankAccountTypeName{
    switch (bankAccountType) {
        default:
        case 1:
            return @"Conta Corrente";
        case 2:
            return @"Conta Poupança";
    }
}

//"ReferenciaBancaria": {
//    "Agencia": 999999999,
//    "AuthenticationToken": null,
//    "BancoId": 1,
//    "CadastroId": 7,
//    "Conta": 999999999,
//    "Descricao": "teste kkkkkkllllllkkkkkkk",
//    "DescricaoBanco": "001 - BANCO DO BRASIL S.A.",
//    "DigitoVerificador": "ik",
//    "ReferenciaBancariaId": 4,
//    "TipoContaBancaria": 1
//}

-(id) initWithJSON:(NSDictionary *)jsonObject{
    self = [self init];
    if (!jsonObject || jsonObject.count == 0) return nil;
    agency = [nonNull([jsonObject valueForKey:@"Agencia"]) intValue];
    bankID = [nonNull([jsonObject valueForKey:@"BancoId"]) intValue];
    registrationID = [nonNull([jsonObject valueForKey:@"CadastroId"]) intValue];
    account = [nonNull([jsonObject valueForKey:@"Conta"]) intValue];
    description = nonNull([jsonObject valueForKey:@"Descricao"]);
    bankDescription = nonNull([jsonObject valueForKey:@"DescricaoBanco"]);
    verificationDigit = nonNull([jsonObject valueForKey:@"DigitoVerificador"]);
    bankReferenceID = [nonNull([jsonObject valueForKey:@"ReferenciaBancariaId"]) intValue];
    bankAccountType = [nonNull([jsonObject valueForKey:@"TipoContaBancaria"]) intValue];
    
    return self;
}

-(id) initWithBankInfo:(CUBankInfo *)copy{
    self = [self init];
    agency = copy.agency;
    bankID = copy.bankID;
    registrationID = copy.registrationID;
    account = copy.account;
    description = copy.description;
    bankDescription = copy.bankDescription;
    verificationDigit = copy.verificationDigit;
    bankReferenceID = copy.bankReferenceID;
    bankAccountType = copy.bankAccountType;
    return self;
}

-(BOOL) isEqual:(id)object{
    CUBankInfo* other = (CUBankInfo*) object;
    if (!other) return NO;
    if (object == self) return YES;
    
    return  (self.bankID == other.bankID) &&
            (self.agency == other.agency) &&
            (self.registrationID == other.registrationID) &&
            (self.account == other.account) &&
            (self.bankReferenceID == other.bankReferenceID) &&
            (self.bankAccountType == other.bankAccountType) &&
            ([self.description isEqualToString:other.description]) &&
            ([self.verificationDigit isEqualToString:other.verificationDigit]) &&
            ([self.bankDescription isEqualToString:other.bankDescription]);
    
}
@end
