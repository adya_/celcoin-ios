//
//  CUUserManager.h
//  CelcoinUsuario
//
//  Created by Adya on 7/17/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CUWallet;
@class CUBankInfo;
@class TSError;
@class CUPaymentReview;

@protocol CUWalletCallback <NSObject>

@optional -(void) onWalletUpdateResult:(BOOL) success withSelectedWallet:(CUWallet*) wallet orError:(TSError*) error;


@optional -(void) onWalletBankReferenceUpdateResult:(BOOL) success withBankReference:(CUBankInfo*) bankReference orError:(TSError*) error;

@optional -(void) onWalletBankReferenceSavedResult:(BOOL) success orError:(TSError*) error;

@optional -(void) onWalletGetBanksListResult:(BOOL) success withList:(NSArray*) banksList orError:(TSError*)error;

@optional -(void) onWalletGetAvailableRechargeValuesResult:(BOOL)success withValues:(NSArray*) values orError:(TSError*)error;

// Responses of any wallet transaction (e.g. Recharge, Withdrawal, Payment, Redeem)
@optional -(void) onWalletTransactionResult:(BOOL) success withProofMessage:(NSString*)message orError:(TSError*) error;

@optional -(void) onWalletPaymentReviewResult:(BOOL) success withReviewInfo:(CUPaymentReview*)review orError:(TSError*) error;

@end


@interface CUWalletManager : NSObject

+ (CUWalletManager*) sharedManager;

@property (readonly) CUWallet* wallet;
@property (readonly) BOOL isOutdatedWallet;
@property (readonly) CUBankInfo* bankReference;

@property id<CUWalletCallback> delegate;

-(void) performUpdateWallet;
-(void) performUpdateBankReference;
-(void) performSetBankReference:(CUBankInfo*) newBankReference;
-(void) performGetBanksList;

-(void) performGetRechargeValuesForPhone:(NSString*) phone andOperator:(int) operatorCode;

-(void) performRechargePhoneWithNumber:(NSString*) phone operator:(int) operatorCode withValue:(int) value;

-(void) performWithdrawal:(float) value;

-(void) performPayment:(float) value withCode:(NSString*) code andDate:(NSString*) date;

-(void) performReviewPaymentWithCode:(NSString*) code;

-(void) performRedeemCouponWithCode:(NSString*) code;

-(CUWallet*) selectWallet:(int) index;

-(void) performTransfer:(float) value toPhoneWithNumber:(NSString*) number;

@end
