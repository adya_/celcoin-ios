//
//  CUUserManager.m
//  CelcoinUsuario
//
//  Created by Adya on 7/17/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUWalletManager.h"
#import "CURequestManager.h"
#import "CUUser.h"
#import "CUWallet.h"
#import "CULoginManager.h"
#import "CUBankInfo.h"
#import "CUPaymentReview.h"
#import "CUTools.h"

@implementation CUWalletManager{
    NSArray* wallets;
    int selectedWalletIndex;
}

@synthesize wallet;
@synthesize isOutdatedWallet;
@synthesize bankReference;

@synthesize delegate;

+ (CUWalletManager*) sharedManager{
    static CUWalletManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

-(void) performUpdateWallet{
    
    CUUser* user = [CULoginManager sharedManager].user;
    [[CURequestManager sharedManager] requestGetWalletForUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
        isOutdatedWallet = wallet && !success;
        if (success){
            NSMutableArray* tmpWallets = [NSMutableArray new];
            for (NSDictionary* w in array) {
                [tmpWallets addObject:[[CUWallet alloc] initWithJSON:w]];
            }
            wallets = [NSArray arrayWithArray:tmpWallets];
            [self selectWallet:selectedWalletIndex];
        }
        if ([delegate respondsToSelector:@selector(onWalletUpdateResult:withSelectedWallet:orError:)])
            [delegate onWalletUpdateResult:(success&&wallet) withSelectedWallet:wallet orError:error];
    }];
}

-(void) performGetRechargeValuesForPhone:(NSString *)phone andOperator:(int)operatorCode{
    CUUser* user = [CULoginManager sharedManager].user;
    [[CURequestManager sharedManager] requestAvailableRechargeValuesForPhoneWithNumber:phone andOperatorCode:operatorCode forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
        if ([delegate respondsToSelector:@selector(onWalletGetAvailableRechargeValuesResult:withValues:orError:)])
            [delegate onWalletGetAvailableRechargeValuesResult:success withValues:array orError:error];
    }];
}

-(void) performRechargePhoneWithNumber:(NSString*) phone operator:(int) operatorCode withValue:(int) value
{
    CUUser* user = [CULoginManager sharedManager].user;
	[[CURequestManager sharedManager] requestRechargePhoneWithNumber:phone operator:operatorCode withValue:value forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if ([delegate respondsToSelector:@selector(onWalletTransactionResult:withProofMessage:orError:)])
            [delegate onWalletTransactionResult:success withProofMessage:(NSString*)object orError:error];
    }];
}


-(CUWallet*) selectWallet:(int)index{
    if (index < 0 || index >= wallets.count)
        return wallet;
    selectedWalletIndex = index;
    wallet = [wallets objectAtIndex:index];
    return wallet;
}

-(void) performUpdateBankReference{
    CUUser* user = [CULoginManager sharedManager].user;
    [[CURequestManager sharedManager] requestUserGetBankInfoForUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if (success)
           bankReference = (CUBankInfo*)object;
        if([delegate respondsToSelector:@selector(onWalletBankReferenceUpdateResult:withBankReference:orError:)])
            [delegate onWalletBankReferenceUpdateResult:success withBankReference:bankReference orError:error];
    }];
}

-(void) performSetBankReference:(CUBankInfo *)newBankReference{
    CUUser* user = [CULoginManager sharedManager].user;
    [[CURequestManager sharedManager] requestUserSetBankInfoWithNickname:newBankReference.description withBankID:newBankReference.bankID agency:newBankReference.agency account:newBankReference.account verificationDigit:newBankReference.verificationDigit bankReferenceID:newBankReference.bankReferenceID bankAccountType:newBankReference.bankAccountType registrationId:newBankReference.registrationID forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, TSError *error) {
        if (success)
            bankReference = [[CUBankInfo alloc] initWithBankInfo:newBankReference];
        if ([delegate respondsToSelector:@selector(onWalletBankReferenceSavedResult:orError:)]){
            [delegate onWalletBankReferenceSavedResult:(success&&bankReference) orError:error];
        }
    }];
}

-(void) performGetBanksList{
    CUUser* user = [CULoginManager sharedManager].user;
    [[CURequestManager sharedManager] requestGetBankListForUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
        if ([delegate respondsToSelector:@selector(onWalletGetBanksListResult:withList:orError:)])
            [delegate onWalletGetBanksListResult:success withList:array orError:error];
    }];
}

-(void) performWithdrawal:(float) value{
    CUUser* user = [CULoginManager sharedManager].user;
    [[CURequestManager sharedManager] requestWithdrawal:value withBankID:bankReference.bankID agency:bankReference.agency account:bankReference.account verificationDigit:bankReference.verificationDigit bankAccountType:bankReference.bankAccountType forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if ([delegate respondsToSelector:@selector(onWalletTransactionResult:withProofMessage:orError:)])
            [delegate onWalletTransactionResult:success withProofMessage:(NSString*)object orError:error];
    }];
}

-(void) performPayment:(float)value withCode:(NSString *)code andDate:(NSString *)date{
    CUUser* user = [CULoginManager sharedManager].user;
    [[CURequestManager sharedManager] requestPayment:value withCode:code andDate:date forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if ([delegate respondsToSelector:@selector(onWalletTransactionResult:withProofMessage:orError:)])
            [delegate onWalletTransactionResult:success withProofMessage:(NSString*)object orError:error];
    }];
}

-(void) performReviewPaymentWithCode:(NSString *)code{
    CUUser* user = [CULoginManager sharedManager].user;
    [[CURequestManager sharedManager] requestCheckPaymentCode:code forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback: ^(BOOL success, NSObject *object, TSError *error){
        CUPaymentReview* review = nil;
        if (success){
            review = (CUPaymentReview*)object;
            review.code = code;
        }
        if ([delegate respondsToSelector:@selector(onWalletPaymentReviewResult:withReviewInfo:orError:)])
        [delegate onWalletPaymentReviewResult:success withReviewInfo:review orError:error];
    }];

}

-(void) performRedeemCouponWithCode:(NSString *)code{
    CUUser* user = [CULoginManager sharedManager].user;
    [[CURequestManager sharedManager] requestRedeemCouponWithCode:code forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if ([delegate respondsToSelector:@selector(onWalletTransactionResult:withProofMessage:orError:)])
            [delegate onWalletTransactionResult:success withProofMessage:(NSString*)object orError:error];
    }];
}

-(void) performTransfer:(float)value toPhoneWithNumber:(NSString *)number{
    CUUser* user = [CULoginManager sharedManager].user;
    [[CURequestManager sharedManager] requestTransfer:value fromNumber:user.phoneNumber to:number forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if ([delegate respondsToSelector:@selector(onWalletTransactionResult:withProofMessage:orError:)])
            [delegate onWalletTransactionResult:success withProofMessage:(NSString*)object orError:error];
    }];
}


@end
