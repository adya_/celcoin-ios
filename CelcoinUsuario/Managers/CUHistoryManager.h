//
//  CUHistoryManager.h
//  CelcoinUsuario
//
//  Created by Adya on 7/20/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TSError;

@protocol CUHistoryCallback <NSObject>

-(void) onGetHistoryResult:(BOOL) success withTransactions:(NSArray*)transactions orError:(TSError*)error;

@end

@interface CUHistoryManager : NSObject

+ (CUHistoryManager*) sharedManager;

@property id<CUHistoryCallback> delegate;

@property NSArray* transactions;

-(void) performGetHistoryInRangeFrom:(NSDate*)fromDate to:(NSDate*)toDate;

@end
