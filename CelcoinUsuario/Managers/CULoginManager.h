//
//  CUUserManager.h
//  CelcoinUsuario
//
//  Created by Adya on 7/9/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CUUser;
@class TSError;
@class CUUserInfo;
@class TSContact;

@protocol CULoginCallback <NSObject>
    @optional -(void) onLoginResult:(BOOL) success withUser:(CUUser*) user orError:(TSError*) error;
@optional -(void) onLogoutResult:(BOOL) success withError:(TSError*) error;
    @optional -(void) onRegistrationResult:(BOOL) success withError:(TSError*) error;
@optional -(void) onSMSConfirmationResult:(BOOL) success withDate:(NSString*)date orError:(TSError*) error;
@optional -(void) onSMSVerificationResult:(BOOL) success orError:(TSError*) error;

@optional -(void) onRegistrationInfoResult:(BOOL) success withInfo:(CUUserInfo*)info orError:(TSError*)error;

@optional -(void) onCelcoinContactCheckedResult:(BOOL) success withCelcoinContacts:(NSDictionary*) celcoinContacts orError:(TSError*) error;
@end



// LoginManager manage login process

// 1. Set loginDelegate to handle auth process callbacks
// 2. Check if manager has some stored token.
// 2.1 If it has then call performLoginWithSavedUser to attempt to restore previous session if it wasn't ended with logout.
// 2.2 If it has not or prev step failed then call performLoginWihtUsername:andPassowrd: to perform login.

@interface CULoginManager : NSObject


+ (CULoginManager*) sharedManager;

@property id<CULoginCallback> delegate;

@property (readonly) CUUser* user;

-(BOOL) hasStoredUser;
-(BOOL) isValidPassword:(NSString*)password;
-(BOOL) isValidUsername:(NSString*)username;
@end


@interface CULoginManager (LoginProcess)

-(void) performLoginWithUsername:(NSString*) username andPassword:(NSString*) password;

-(void) performLoginWithSavedUser;
-(void) logout;

-(void) performLogout;


-(void) performCheckCelcoinContacts:(NSArray*) contacts;

-(void) performUpdateAccountInfo;

@end

@interface CULoginManager (RegistrationProcess)

-(BOOL) isValidSMSPassword:(NSString*) sms;
-(BOOL) isValidSMSDate:(NSString*) smsDate;

-(void) performRegistrationWithPhone:(NSString *)phone password:(NSString *)password SMSPassword:(NSString *)sms andSMSDate:(NSString *)smsDate loginOnSuccess:(BOOL) shouldLogin;

-(void) performSMSConfirmationWithPhone:(NSString*) phone;
-(void) performSMSVerificationWithSMS:(NSString*) sms andSMSDate:(NSString*) date forPhoneNumber:(NSString*) phoneNumber;

-(void) performCompleteRegistrationWithOperatorId:(int) operatorId;

-(void) performCompleteRegistrationWithName:(NSString*)name andCPF:(NSString*)cpf;

@end