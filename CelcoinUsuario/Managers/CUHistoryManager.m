//
//  CUHistoryManager.m
//  CelcoinUsuario
//
//  Created by Adya on 7/20/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUHistoryManager.h"
#import "CURequestManager.h"
#import "TSUtils.h"
#import "CUUser.h"
#import "CULoginManager.h"
#import "CUTransaction.h"


@implementation CUHistoryManager

+ (CUHistoryManager*) sharedManager{
    static CUHistoryManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

@synthesize delegate;
@synthesize transactions;

-(void) performGetHistoryInRangeFrom:(NSDate*)fromDate to:(NSDate*)toDate
{
    CUUser* user = [CULoginManager sharedManager].user;
    NSString* from = [TSUtils convertDate:fromDate toStringWithFormat:@"yyyyMMdd"];
    NSString* to = [TSUtils convertDate:toDate toStringWithFormat:@"yyyyMMdd"];
	[[CURequestManager sharedManager] requestUserTransactionHistoryInRangeFrom:from to:to forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
        if (success){
            NSMutableArray* tmp = [NSMutableArray new];
            for (int i = (int)array.count - 1; i >= 0; --i) {
                NSDictionary* tr = array[i];
                CUTransaction* transaction = [[CUTransaction alloc] initWithJSON:tr];
                [tmp addObject:transaction];
            }
            transactions = [NSArray arrayWithArray:tmp];
            
        }
        if ([delegate respondsToSelector:@selector(onGetHistoryResult:withTransactions:orError:)])
            [delegate onGetHistoryResult:success withTransactions:transactions orError:nil];
    }];
}

@end
