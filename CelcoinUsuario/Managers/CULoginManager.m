//
//  CUUserManager.m
//  CelcoinUsuario
//
//  Created by Adya on 7/9/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CULoginManager.h"

#import "CUUser.h"
#import "TSError.h"
#import "CURequestManager.h"
#import <CommonCrypto/CommonCryptor.h>
#import "CUTools.h"
#import "CUUserInfo.h"
#import "TSContactsManager.h"
#import "TSUtils.h"

#define USER_TOKEN @"userToken"
#define USER_AUTH_TOKEN @"userAuthToken"
#define USER_USERNAME @"userEmail"

@implementation CULoginManager

@synthesize delegate;
@synthesize user;

+ (CULoginManager*) sharedManager{
    static CULoginManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

-(id) init{
    self = [super init];
    [self loadUserData];
    return self;
}
-(void) clearUserData{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    [prefs removeObjectForKey:USER_TOKEN];
    [prefs removeObjectForKey:USER_AUTH_TOKEN];
    [prefs synchronize];
}

-(void) saveUserData{
    if (!user) return;
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:user.phoneNumber forKey:USER_USERNAME];
    [prefs setObject:user.accessToken forKey:USER_AUTH_TOKEN];
    [prefs setObject:user.token forKey:USER_TOKEN];
    [prefs synchronize];
}

-(void) loadUserData{
  //  NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
}


-(BOOL) hasStoredUser{
    return ((user != nil) && (user.token != nil) && (user.accessToken != nil));
}

-(BOOL) isValidUsername:(NSString *)username{
    return [CUTools isValidPhone:username] || [TSUtils isValidEmail:username];
}

-(BOOL) isValidPassword:(NSString *)password{
    password = [password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return password != nil && password.length >= 6;
}

@end

@implementation CULoginManager (LoginProcess)

-(void) performLoginWithUsername:(NSString *)userName andPassword:(NSString *)password{
    NSString* encryptedPass = [self doCipher:password];
    [[CURequestManager sharedManager] requestLoginForUserWithPhone:userName andPassword: encryptedPass withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
        if (success){
            CUUser* authorizedUser = (CUUser*)object;
            user = [[CUUser alloc] initWithUser:authorizedUser];
            user.phoneNumber = userName;
            [self saveUserData];
        }
        else{
            user = nil;
        }
        if ([delegate respondsToSelector:@selector(onLoginResult:withUser:orError:)])
            [delegate onLoginResult:success withUser:user orError:error];
    }];
}
// Unused in Celcoin
-(void) performLoginWithSavedUser{
    if (![self hasStoredUser]){
        if ([delegate respondsToSelector:@selector(onLoginResult:withUser:orError:)])
            [delegate onLoginResult:NO withUser:nil orError:nil];
        return;
    }
    // if there is stored user it has been already loaded.
    if ([delegate respondsToSelector:@selector(onLoginResult:withUser:orError:)])
        [delegate onLoginResult:YES withUser:user orError:nil];
}

-(void) performLogout{
    [[CURequestManager sharedManager] requestLogoutForUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, TSError *error) {
        if ([delegate respondsToSelector:@selector(onLogoutResult:withError:)])
            [delegate onLogoutResult:success withError:error];
    }];
}

-(void) performCheckCelcoinContacts:(NSArray*) contacts{
    NSMutableDictionary* phoneContactPairs = [NSMutableDictionary new];
    NSMutableArray* plainPhones = [NSMutableArray new];
    for (TSContact* contact in contacts) {
        NSArray* phones = [contact.auxPhones valueForKey:@"number"];
        for (NSString* phone in phones) {
            NSString* formattedPhone = [CUTools formattedLocalNumber:phone];
            [plainPhones addObject:formattedPhone];
            [phoneContactPairs setObject:contact forKey:formattedPhone];

        }
    }
    [[CURequestManager sharedManager] requestCheckContacts:plainPhones forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSArray *array, TSError *error) {
        NSMutableDictionary* celcoinContacts = [NSMutableDictionary new];
        if (success && array.count > 0){
            for (NSString* phone in array) {
                NSString* formattedPhone = [CUTools formattedLocalNumber:phone];
                TSContact* linkedContact = [phoneContactPairs objectForKey:formattedPhone];
                TSContact* c = [celcoinContacts objectForKey:linkedContact.fullName];
                if (!c)
                    c = [TSContact new];
                c.firstName = linkedContact.firstName;
                c.lastName = linkedContact.lastName;
                c.middleName = linkedContact.middleName;
                NSMutableArray* phones = [NSMutableArray new];
                if (c.auxPhones){
                    [phones addObjectsFromArray:c.auxPhones];
                }
                TSPhone* p = [TSPhone new];
                p.number = formattedPhone;
                [phones addObject:p];
                c.auxPhones = [NSArray arrayWithArray:phones];
                [celcoinContacts setObject:c forKey:c.fullName];
            }
        }
        if ([delegate respondsToSelector:@selector(onCelcoinContactCheckedResult:withCelcoinContacts:orError:)])
            [delegate onCelcoinContactCheckedResult:success withCelcoinContacts:[NSDictionary dictionaryWithDictionary:celcoinContacts] orError:error];
    }];
}


-(void) performUpdateAccountInfo{
    [[CURequestManager sharedManager] requestRegistrationInfoForUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, NSObject *object, TSError *error) {
       if (success)
           user.info = (CUUserInfo*)object;
        if ([delegate respondsToSelector:@selector(onRegistrationInfoResult:withInfo:orError:)])
            [delegate onRegistrationInfoResult:success withInfo:user.info orError:error];
    }];
}

-(void) logout{
    NSString* username = user.phoneNumber;
    user = [CUUser new];
    user.phoneNumber = username;
    [self clearUserData];
}

- (NSString*) doCipher:(NSString*)encryptValue {
    
    const void *vplainText;
    size_t plainTextBufferSize = [encryptValue length];
    vplainText = (const void *) [encryptValue UTF8String];
    CCCryptorStatus ccStatus;
    uint8_t *bufferPtr = NULL;
    size_t bufferPtrSize = 0;
    size_t movedBytes = 0;
    
    bufferPtrSize = (plainTextBufferSize + kCCBlockSizeDES) & ~(kCCBlockSizeDES - 1);
    bufferPtr = malloc( bufferPtrSize * sizeof(uint8_t));
    memset((void *)bufferPtr, 0x0, bufferPtrSize);
    
    Byte iv [] = {0xb8, 0xf4, 0xad, 0xc5, 0x97, 0x30, 0xe9, 0x85}; //B8 F4 AD C5 97 30 E9 85
    Byte key[] = {0x2a, 0x5e, 0x7f, 0x14, 0xb6, 0xac, 0x80, 0x1d}; // 2A 5E 7F 14 B6 AC 80 1D
    ccStatus = CCCrypt(kCCEncrypt,
                       kCCAlgorithmDES,
                       kCCOptionPKCS7Padding,
                       key,
                       kCCKeySizeDES,
                       iv,
                       vplainText,
                       plainTextBufferSize,
                       (void *)bufferPtr,
                       bufferPtrSize,
                       &movedBytes);
    
    Byte *byteData = bufferPtr;
    NSString* result = [self byteString:byteData[0]];
    for (int i = 1 ; i < movedBytes; i ++)
    {
        result = [result stringByAppendingString:[self byteString:byteData[i]]];
    }
    result = [result uppercaseString];
    return result;
}

-(NSString*) byteString:(Byte) byte{
    if (byte < 16)
        return [NSString stringWithFormat:@"0%X", byte];
    else
        return [NSString stringWithFormat:@"%X", byte];
}

@end


@implementation CULoginManager (RegistrationProcess)

-(void) performRegistrationWithPhone:(NSString *)phone password:(NSString *)password SMSPassword:(NSString *)sms andSMSDate:(NSString *)smsDate loginOnSuccess:(BOOL) shouldLogin{
    
    NSString* encryptedPass = [self doCipher:password];
    [[CURequestManager sharedManager] requestRegisterUserWithPhone:phone password:encryptedPass SMSPassword:sms andSMSDate:smsDate withResponseCallback:^(BOOL success, TSError *error) {
        if ([delegate respondsToSelector:@selector(onRegistrationResult:withError:)])
            [delegate onRegistrationResult:success withError:error];
        
        if (success && shouldLogin){
            [self performLoginWithUsername:phone andPassword:password];
        }
        if (!success){
            user = nil;
        }
    }];
}

-(void) performSMSConfirmationWithPhone:(NSString*) phone
{
	[[CURequestManager sharedManager] requestUserConfirmSMS:phone withResponseCallback:^(BOOL success, NSObject* date, TSError *error) {
            if ([delegate respondsToSelector:@selector(onSMSConfirmationResult:withDate:orError:)])
                [delegate onSMSConfirmationResult:success withDate:(NSString*)date orError:error];
    }];
}

-(void) performSMSVerificationWithSMS:(NSString*) sms andSMSDate:(NSString*) date forPhoneNumber:(NSString*) phoneNumber{
    [[CURequestManager sharedManager] requestVerifySMSPassword:sms andSMSDate:date forPhoneNumber:phoneNumber withResponseCallback:^(BOOL success, TSError *error) {
        if ([delegate respondsToSelector:@selector(onSMSVerificationResult:orError:)])
            [delegate onSMSVerificationResult:success orError:error];
    }];
}

-(void) performCompleteRegistrationWithOperatorId:(int) operatorId{
    [[CURequestManager sharedManager] requestCompleteUserRegistrationWithOperatorId:operatorId forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, TSError *error) {
        if ([delegate respondsToSelector:@selector(onRegistrationResult:withError:)])
            [delegate onRegistrationResult:success withError:error];
    }];
}

-(void) performCompleteRegistrationWithName:(NSString*)name andCPF:(NSString*)cpf{
    [[CURequestManager sharedManager] requestCompleteUserRegistrationWithName:name andCPF:cpf forUserWithToken:user.token andAccessToken:user.accessToken withResponseCallback:^(BOOL success, TSError *error) {
        if ([delegate respondsToSelector:@selector(onRegistrationResult:withError:)])
            [delegate onRegistrationResult:success withError:error];
    }];
}

-(BOOL) isValidSMSPassword:(NSString *)sms{
    return sms.length == 6;
}

-(BOOL) isValidSMSDate:(NSString *)smsDate{
    NSString* smsDateClean = [CUTools extractNumber:smsDate];
    return smsDateClean.length == 14;
}


@end