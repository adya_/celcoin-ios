//
//  CUTransactionCell.m
//  CelcoinUsuario
//
//  Created by Adya on 7/20/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUTransactionCell.h"
#import "CUTransaction.h"
#import "TSUtils.h"


@interface CUTransactionCell ()
@property (weak, nonatomic) IBOutlet UILabel *lDescr;
@property (weak, nonatomic) IBOutlet UILabel *lValue;
@property (weak, nonatomic) IBOutlet UILabel *lDate;
@property (weak, nonatomic) IBOutlet UILabel *lStoreName;

@end

@implementation CUTransactionCell{
    CUTransaction* transaction;
}

-(void) setTransaction:(CUTransaction *)_transaction{
    transaction = _transaction;
    [self updateCell];
}

-(void) updateCell{
    self.lDescr.text = transaction.description;
    self.lStoreName.text = transaction.storeName;
    self.lDate.text = transaction.date;
    if (transaction.value < 0)
        self.lValue.text = [NSString stringWithFormat:@"-R $%.2f", -transaction.value];
    else
        self.lValue.text = [NSString stringWithFormat:@"R $%.2f", transaction.value];

    
    self.lValue.textColor = (transaction.value < 0 ? colorRGB(212, 0, 32) : colorRGB(59, 159, 55));

}

+(CGFloat) height{
    return 65.0f;
}


@end
