//
//  CUTransactionCell.h
//  CelcoinUsuario
//
//  Created by Adya on 7/20/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CUTransaction;

@interface CUTransactionCell : UITableViewCell

-(void) setTransaction:(CUTransaction*) transaction;

+ (CGFloat) height;
@end
