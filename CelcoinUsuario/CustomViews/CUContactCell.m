//
//  CUContactCell.m
//  CelcoinUsuario
//
//  Created by Adya on 7/28/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUContactCell.h"
#import "TSContactsManager.h"

@interface CUContactCell ()
@property (weak, nonatomic) IBOutlet UIImageView *ivPicture;
@property (weak, nonatomic) IBOutlet UILabel *lTitle;
@property (weak, nonatomic) IBOutlet UIImageView *ivCelcoin;

@end

@implementation CUContactCell{
    TSContact* _contact;
}

-(void) setContact:(TSContact*)contact{
    _contact = contact;
    [self styleView];
    [self updateCell];
    [self setIsCelcoin:NO];

}

-(BOOL) isCelcoin {
    return !self.ivCelcoin.hidden;
}

-(void) setIsCelcoin:(BOOL)isCelcoin{
    self.ivCelcoin.hidden = !isCelcoin;
}

-(void) styleView{
    self.ivPicture.layer.cornerRadius = self.ivPicture.frame.size.height/2;
    self.ivPicture.layer.masksToBounds = YES;
}

-(void) updateCell{
    self.ivPicture.image = ((_contact.picture)?_contact.picture:[CUContactCell defaultImage]);

    self.lTitle.text = _contact.fullName;
}

+(UIImage*) defaultImage{
    return [UIImage imageNamed:@"avatar"];
}

+(CGFloat) height{
    return 67;
}

@end
