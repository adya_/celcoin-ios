//
//  CUContactCell.h
//  CelcoinUsuario
//
//  Created by Adya on 7/28/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TSContact;

@interface CUContactCell : UITableViewCell


-(void) setContact:(TSContact*)contact;

@property BOOL isCelcoin;

+(CGFloat) height;

@end
