#import "TSNotifier.h"
#import "MBProgressHUD.h"
#import "TSError.h"
#import "TSUtils.h"
#import "LMAlertView.h"


// Helper to delegate callbacks from buttons
@interface TSAlertViewDelegate  : NSObject <UIAlertViewDelegate>

+ (TSAlertViewDelegate*) sharedDelegate;
-(void) setCallbacksAccept:(ButtonCallback)accept andCancel:(ButtonCallback) cancel;

@end


@implementation TSNotifier

+(void) log:(NSString*) message{
    [self logWithTitle:APP_NAME message:message];
}

+(void) logWithTitle:(NSString*) title message:(NSString*) msg{
    NSLog(@"%@ : %@", title, msg);
}

+(void) logMethod:(NSString *)method{
    [self logWithTitle:@"NOT IMPLEMENTED" message:method];
}
@end


@implementation TSNotifier (Alerts)

+(void) alert:(NSString*) message{
    [self alert:message acceptButton:nil];
}
+(void) alert:(NSString *)message acceptButton:(NSString *)ok{
    [self alert:message acceptButton:ok acceptBlock:nil];
}
+(void) alert:(NSString *)message acceptButton:(NSString *)ok acceptBlock:(ButtonCallback)acceptBlock{
    [self alert:message acceptButton:ok acceptBlock:acceptBlock cancelButton:nil cancelBlock:nil];
}
+(void) alert:(NSString*) message acceptButton:(NSString*) ok acceptBlock:(ButtonCallback) acceptBlock cancelButton:(NSString*) cancel cancelBlock:(ButtonCallback) cancelBlock{
    [self alertWithTitle:APP_NAME message:message acceptButton:ok acceptBlock:acceptBlock cancelButton:cancel cancelBlock:cancelBlock];
}

+(void) alertWithTitle:(NSString*) title message: (NSString*) msg{
    [self alertWithTitle:title message:msg acceptButton:nil acceptBlock:nil];
}
+(void) alertWithTitle:(NSString *)title message:(NSString *)msg acceptButton:(NSString*) ok acceptBlock:(ButtonCallback) acceptBlock{
    [self alertWithTitle:title message:msg acceptButton:ok acceptBlock:acceptBlock cancelButton:nil cancelBlock:nil];
}
+(void) alertWithTitle:(NSString *)title message:(NSString *)msg acceptButton:(NSString*) ok acceptBlock:(ButtonCallback) acceptBlock cancelButton:(NSString*) cancel cancelBlock:(ButtonCallback) cancelBlock{
    [self logWithTitle:title message:msg];
    TSAlertViewDelegate* delegate = [TSAlertViewDelegate sharedDelegate];
    [delegate setCallbacksAccept:acceptBlock andCancel:cancelBlock];
    if (!ok) ok = @"OK";
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:title message:msg delegate: delegate cancelButtonTitle:cancel otherButtonTitles:ok, nil];
//    LMAlertView* alert = [[LMAlertView alloc] initWithTitle:title message:msg delegate:delegate cancelButtonTitle:cancel otherButtonTitles:ok, nil];
//    UILabel* label = ((UILabel*)[alert.contentView.subviews objectAtIndex:1]);
//    label.textAlignment = NSTextAlignmentCenter;
    [alert show];
}

+(void) alertWithTitle:(NSString*) title message:(NSString*) msg withAlignment:(NSTextAlignment) alignment acceptButton:(NSString*) ok acceptBlock:(ButtonCallback) acceptBlock cancelButton:(NSString*) cancel cancelBlock:(ButtonCallback) cancelBlock{
    [self logWithTitle:title message:msg];
    TSAlertViewDelegate* delegate = [TSAlertViewDelegate sharedDelegate];
    [delegate setCallbacksAccept:acceptBlock andCancel:cancelBlock];
    if (!ok) ok = @"OK";
    LMAlertView* alert = [[LMAlertView alloc] initWithTitle:title message:msg delegate:delegate cancelButtonTitle:cancel otherButtonTitles:ok, nil];
    ((UILabel*)[alert.contentView.subviews objectAtIndex:1]).textAlignment = alignment;
//    [[UIAlertView alloc] initWithTitle:title message:msg delegate: delegate cancelButtonTitle:cancel otherButtonTitles:ok, nil];
    
    [alert show];

}


+(void) alertError:(TSError *)error{
    [self alertError:error acceptButton:nil acceptBlock:nil];
}

+(void) alertError:(TSError*)error acceptButton:(NSString*)ok acceptBlock:(ButtonCallback) acceptBlock{
    [self alertError:error acceptButton:ok acceptBlock:acceptBlock cancelButton:nil cancelBlock:nil];
}
+(void) alertError:(TSError*)error acceptButton:(NSString*)ok acceptBlock:(ButtonCallback) acceptBlock cancelButton:(NSString*)cancel cancelBlock:(ButtonCallback) cancelBlock{
    if (!error.title || [error.title isEqualToString:@""])
        [self alertWithTitle:APP_NAME message:error.description acceptButton:ok acceptBlock:acceptBlock cancelButton:cancel cancelBlock:cancelBlock];
    else
        [self alertWithTitle:error.title message:error.description acceptButton:ok acceptBlock:acceptBlock cancelButton:cancel cancelBlock:cancelBlock];
}

@end

@implementation TSNotifier (Notifications)

+(void) notify:(NSString*) message onView:(UIView*) view
{
    [self notify:message onView:view forTimeInterval:TS_NOTIFICATION_TIME_NORMAL];
}
+(void) notify:(NSString*) message onView:(UIView*) view forTimeInterval:(TSNotificationTimeInterval) time
{
    [self hideNotificationOnView:view];
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.margin = 10.0f;
    hud.yOffset = view.frame.size.height/3;
    hud.labelText = message;
    hud.userInteractionEnabled = NO;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:time];
}

+(void) notifyError:(TSError*) error onView:(UIView*) view
{
    [self notifyError:error onView:view forTimeInterval:TS_NOTIFICATION_TIME_NORMAL];
}

+(void) notifyError:(TSError*) error onView:(UIView*) view forTimeInterval:(TSNotificationTimeInterval) time
{
    if (!error.title || [error.title isEqualToString:@""])
        [self notify:[NSString stringWithFormat: @"%@", error.description] onView:view forTimeInterval:time];
    else
        [self notify:[NSString stringWithFormat: @"%@ : %@", error.title, error.description] onView:view forTimeInterval:time];
}

+(void) hideNotificationOnView:(UIView*) view{
    [MBProgressHUD hideHUDForView:view animated:YES];
}


@end


@implementation TSNotifier (ProgressBars)

+(void) showProgressOnView:(UIView*)view withMessage:(NSString*)message{
    [self hideNotificationOnView:view];
    MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = message;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.dimBackground = YES;
    hud.removeFromSuperViewOnHide = YES;
    
}

+(void) hideProgressOnView:(UIView*)view{
    [MBProgressHUD hideHUDForView:view animated:YES];
}

@end

@implementation TSAlertViewDelegate{
    ButtonCallback accept;
    ButtonCallback cancel;
}

+ (TSAlertViewDelegate*) sharedDelegate{
    static TSAlertViewDelegate* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

-(void) setCallbacksAccept:(ButtonCallback)_accept andCancel:(ButtonCallback)_cancel{
    accept = _accept;
    cancel = _cancel;
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            if (cancel)
                cancel();
            else
                [TSNotifier log:@"Cancel callback wasn't defined."];
            break;
        case 1:
            if (accept)
                accept();
            else
                [TSNotifier log:@"Accept callback wasn't defined."];
            break;
        default:
                [TSNotifier log:@"Undefined button."];
            break;
    }
}

@end