#import <Foundation/Foundation.h>

#ifndef TS_ERRORS
#define TS_ERRORS
// Error types
#define TS_ERROR_INTERNAL -1

#endif

@interface TSError : NSObject

// Contains general info about error
@property NSString* title;

// Contains error details
@property NSString* description;


// Contains error code to identify error type
@property NSInteger code;

- (TSError*) initWithCode:(NSInteger)code;
- (TSError*) initWithCode:(NSInteger)code Title:(NSString*)title;
- (TSError*) initWithCode:(NSInteger)code Title:(NSString *)title andDescription:(NSString*) description;
-(TSError*) initWithError:(NSError*) error;


+ (TSError*) errorWithCode:(NSInteger)code;
+ (TSError*) errorWithCode:(NSInteger)code Title:(NSString*)title;
+ (TSError*) errorWithCode:(NSInteger)code Title:(NSString *)title andDescription:(NSString*) description;
+ (TSError*) errorWithError:(NSError*) error;

@end
