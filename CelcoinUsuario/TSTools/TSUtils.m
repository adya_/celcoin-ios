//
//  TSUtils.m
//  Created by Adya on 13/01/2015.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "TSUtils.h"

#import <SystemConfiguration/CaptiveNetwork.h>
#import <ifaddrs.h>
#import <arpa/inet.h>
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>
#include <sys/ioctl.h>

#define DATE_PATTERN @"(.*)-(.*)-(.*)T(.*):(.*):(.*)\\.(.*)"
#define REGEX_URL_PATTERN @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
#define REGEX_EMAIL_PATTERN @"^([A-Z0-9a-z._%+-]+)@((?:[A-Za-z0-9-]+)\\.)+([A-Za-z]{2,6})$"

@implementation TSUtils

+(NSString*) formatDateTime:(NSString*)dateTime{
    if (!dateTime) return nil;
    NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:DATE_PATTERN options:0 error:nil];
    
    return [regex stringByReplacingMatchesInString:dateTime options:0 range:NSMakeRange(0, dateTime.length) withTemplate:[NSString stringWithFormat:@"%@-%@-%@ %@:%@",@"$2",@"$3",@"$1",@"$4",@"$5"]];
}

+(NSString*) formatToLocalFromUTCDateTime:(NSString*)dateTime{
    NSString* dTime = [self formatDateTime:dateTime];
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [df_utc setDateFormat:@"MM-dd-yyyy HH:mm"];
    
    NSDate* date = [df_utc dateFromString:dTime];
    
    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    [df_local setTimeZone:[NSTimeZone defaultTimeZone]];
    [df_local setDateFormat:@"MM-dd-yyyy HH:mm"];
    return [df_local stringFromDate:date];
}

+(NSString*) convertDate:(NSDate*)date toStringWithFormat:(NSString*) dateFormat{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateFormat];
    return [formatter stringFromDate:date];
}

+(NSDate*) convertString:(NSString*)date toDateWithFormat:(NSString*) dateFormat
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateFormat];
    return [formatter dateFromString:date];
}

+(NSString*) formatDateString:(NSString*) string withFormat:(NSString*)sourceFormat toFormat:(NSString*) destFormat{
    NSDate* date = [self convertString:string toDateWithFormat:sourceFormat];
    if (!date) return nil;
    return [self convertDate:date toStringWithFormat:destFormat];
}



+ (NSString *)trim:(NSString *)target{
    if (![TSUtils nonNull:target]) return nil;
    return [target stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+(NSString*) appName{
    return [[[NSBundle mainBundle] infoDictionary] valueForKey:(NSString*)kCFBundleNameKey];
}

+(id) nonNull:(id)target{
    return ([[NSNull null] isEqual:target] ? nil : target);
}

+(UIColor*) colorWithRed:(int)red green:(int)green blue:(int)blue{
    return [TSUtils colorWithRed:red green:green blue:blue alpha:255];
}

+(UIColor*) colorWithRed:(int)red green:(int)green blue:(int)blue alpha:(int)alpha{
    return [UIColor colorWithRed:((float)red)/255.0f green:((float)green)/255.0f blue:((float)blue)/255.0f alpha:((float)alpha)/255.0f];
}

+ (BOOL) isValidURL: (NSString *) url {
    NSPredicate* urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", REGEX_URL_PATTERN];
    return [urlTest evaluateWithObject:url];
}

+(BOOL) isValidEmail:(NSString*) email{
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", REGEX_EMAIL_PATTERN];
    return [emailTest evaluateWithObject:email];
}

+(NSString*) getIPAddress{
    @try {
        // Set a string for the address
        NSString *IPAddress;
        // Set up structs to hold the interfaces and the temporary address
        struct ifaddrs *Interfaces;
        struct ifaddrs *Temp;
        struct sockaddr_in *s4;
        char buf[64];
        
        // If it's 0, then it's good
        if (!getifaddrs(&Interfaces))
        {
            // Loop through the list of interfaces
            Temp = Interfaces;
            
            // Run through it while it's still available
            while(Temp != NULL)
            {
                // If the temp interface is a valid interface
                if(Temp->ifa_addr->sa_family == AF_INET)
                {
                    // Check if the interface is Cell
//                    if([[NSString stringWithUTF8String:Temp->ifa_name] isEqualToString:@"pdp_ip0"])
//                    {
                        s4 = (struct sockaddr_in *)Temp->ifa_addr;
                        
                        if (inet_ntop(Temp->ifa_addr->sa_family, (void *)&(s4->sin_addr), buf, sizeof(buf)) == NULL) {
                            // Failed to find it
                            IPAddress = nil;
                        } else {
                            // Got the Cell IP Address
                            IPAddress = [NSString stringWithUTF8String:buf];
                        }
//                    }
                }
                
                // Set the temp value to the next interface
                Temp = Temp->ifa_next;
            }
        }
        
        // Free the memory of the interfaces
        freeifaddrs(Interfaces);
        
        // Check to make sure it's not empty
        if (IPAddress == nil || IPAddress.length <= 0) {
            // Empty, return not found
            return nil;
        }
        
        // Return the IP Address of the WiFi
        return IPAddress;
    }
    @catch (NSException *exception) {
        // Error, IP Not found
        return nil;
    }
}


@end
