
// TSRequestManager designed to sped up implmentation of the api reqeusts to the server.

// Therefore it has some restrictions and requirenments

// 1. Subclass this interface. Do NOT use it directly.

// 2. You must override and implement all methods under "MustBeImplemented" Category which are derived from protocol "TSRequestHelperMethods" (do not confuse with category "HelperMethods") to provide connectivity with your server.

// 3. Set up your manager's headers (accept, content-types, timeout, etc.) by overriding prepareManager method.
// Note: you could override initManager to fully customize initialization. (But do it for your risk).

// 4. Declare your request methods and implement them using manager's methods POST, DELETE, etc.

// 5. For handling responses use predefined callback types (see below).


#import <Foundation/Foundation.h>

@class AFHTTPRequestOperationManager;
@class AFHTTPRequestOperation;
@class TSError;

// Callbacks for different types of responses (void, object, array)
typedef void(^OperationResponseCallback)(BOOL success, TSError* error);
typedef void(^ObjectResponseCallback)(BOOL success, NSObject* object, TSError* error);
typedef void(^ArrayResponseCallback)(BOOL success, NSArray* array, TSError* error);

// This protocol must be implemented in subclasses to provide connectivity with specific server.
@protocol TSRequestHelperMethods

 // returns base url of server api (thus not a whole url but just domen name).
-(NSString*) getServerUrl;

// returns param name which contains attached files data.
-(NSString*) getFilesParamName;

 // returns name of the entity which will wrap json-object.
-(NSString*) getWrapperEntityName;

// returns paarm name of the field with succesful response json-object.
-(NSString*) getResponseParamName;

// returns param name of the field with error code.
-(NSString*) getErrorCodeParamName;

// returns param name of the field with error message.
-(NSString*) getErrorMessageParamName;

// returns timeout given to all requests.
-(NSTimeInterval) getRequestTimeout;

 // checks whether the response is successful or not.
-(BOOL) isSuccessfulResponse:(NSDictionary*) response;
@end

@interface TSRequestManager : NSObject {
    AFHTTPRequestOperationManager* afManager;
}

+ (id) sharedManager;

-(void) requestImageFromURL:(NSString*)url forImageView:(UIImageView*) imageView withPlaceholder:(UIImage*) placeholder success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                    failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure;

-(void) requestImageFromURL:(NSString*)url
                forImageView:(UIImageView*) imageView
                withPlaceholder:(UIImage*) placeholder;

-(void) requestImageFromURL:(NSString*)url
               forImageView:(UIImageView*) imageView
            withPlaceholder:(UIImage*) placeholder animated:(BOOL)animated;


// simple POST with body params
-(void) POST:(NSString*)requestUrl
    withBody:(NSDictionary*) body
  andHeaders:(NSDictionary*) headers
successBlock:(void(^)(AFHTTPRequestOperation *operation, id responseObject))success
failureBlock:(void(^)(AFHTTPRequestOperation *operation, NSError *error)) failure;

// multipart POST with files.
-(void)POST:(NSString*)requestUrl
   withData:(NSData*) fileData
      named:(NSString*) fileName
withMimeType:(NSString*) mimeType
 andHeaders:(NSDictionary*) headers
successBlock:(void(^)(AFHTTPRequestOperation *operation, id responseObject))success
failureBlock:(void(^)(AFHTTPRequestOperation *operation, NSError *error)) failure;

-(void) DELETE:(NSString*)requestUrl
      withBody:(NSDictionary*) body
    andHeaders:(NSDictionary*) headers
  successBlock:(void(^)(AFHTTPRequestOperation *operation, id responseObject))success
  failureBlock:(void(^)(AFHTTPRequestOperation *operation, NSError *error)) failure;
@end

@interface TSRequestManager (HelperMethods)
-(NSDictionary*) wrapWithEntity:(NSDictionary*) params;

-(void) setValue:(NSString*) value forHeaderField:(NSString*) header;
-(void) setAcceptTypes:(NSArray*) types;
-(void) setContentTypes:(NSArray*) types;
-(TSError*) parseError:(NSDictionary*) errorResponse;
-(TSError*) parseNSError:(NSError*) error;
@end

@interface TSRequestManager (MustBeImplemented) <TSRequestHelperMethods>
// These methods are not "public" so their declarations are hidden.
@end
