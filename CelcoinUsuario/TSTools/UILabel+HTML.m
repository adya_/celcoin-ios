//
//  TSHtmlLabel.m
//  CelcoinUsuario
//
//  Created by Adya on 6/23/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "UILabel+HTML.h"

@implementation UILabel (TSHtmlLabel)

- (void) setHtml: (NSString*) html
{
    NSError *err = nil;
    self.attributedText =
    [[NSAttributedString alloc]
     initWithData: [html dataUsingEncoding:NSUTF8StringEncoding]
     options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
     documentAttributes: nil
     error: &err];
    if(err)
        NSLog(@"Unable to parse label text: %@", err);
    [self sizeToFit];
}


@end
