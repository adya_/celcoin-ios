
#import "TSError.h"
#import "TSUtils.h"

@implementation TSError

@synthesize title;
@synthesize description;
@synthesize code;


-(TSError*) initWithCode:(NSInteger)errorCode{
    return [self initWithCode:errorCode Title:@"Error"];
}

-(TSError*) initWithCode:(NSInteger)errorCode Title:(NSString*)errorTitle{
    return [self initWithCode:errorCode Title:errorTitle andDescription:nil];
}

-(TSError*) initWithCode:(NSInteger)errorCode Title:(NSString *)errorTitle andDescription:(NSString*) errorDescription{
    self = [self init];
    code = errorCode;
    title = errorTitle;
    description = errorDescription;
    return self;
}

-(TSError*) initWithError:(NSError *)error{
    return [self initWithCode:error.code Title:APP_NAME andDescription:error.localizedDescription];
}

+(TSError*) errorWithCode:(NSInteger)code{
    return [self errorWithCode:code Title:@""];
}

+(TSError*) errorWithCode:(NSInteger)code Title:(NSString*)title{
    return [self errorWithCode:code Title:title andDescription:@""];
}

+(TSError*) errorWithCode:(NSInteger)code Title:(NSString *)title andDescription:(NSString *)description{
    return [[TSError alloc] initWithCode:code Title:title andDescription:description];
}

+(TSError*) errorWithError:(NSError *)error{
    return [[TSError alloc] initWithError:error];
}
@end
