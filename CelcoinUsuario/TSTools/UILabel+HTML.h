//
//  TSHtmlLabel.h
//  CelcoinUsuario
//
//  Created by Adya on 6/23/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (TSHtmlLabel)

-(void) setHtml:(NSString*) html;
@end
