@class TSError;

typedef void(^ButtonCallback)();

typedef NS_ENUM(NSInteger, TSNotificationTimeInterval){
    TS_NOTIFICATION_TIME_SHORT = 3,
    TS_NOTIFICATION_TIME_NORMAL = 5,
    TS_NOTIFICATION_TIME_LONG = 8
};

#define TSLog(title, msg) [TSNotifier logWithTitle:title message:msg]

// Logs and notifies (popup) messages
@interface TSNotifier : NSObject
+(void) log:(NSString*) message;
+(void) logWithTitle:(NSString*) title message:(NSString*) msg;
+(void) logMethod:(NSString*) method;
@end

@interface TSNotifier (Alerts)

+(void) alert:(NSString*) message;
+(void) alert:(NSString*) message acceptButton:(NSString*) ok acceptBlock:(ButtonCallback) acceptBlock;
+(void) alert:(NSString*) message acceptButton:(NSString*) ok acceptBlock:(ButtonCallback) acceptBlock cancelButton:(NSString*) cancel cancelBlock:(ButtonCallback) cancelBlock;

+(void) alertWithTitle:(NSString*) title message:(NSString*) msg;
+(void) alertWithTitle:(NSString*) title message:(NSString*) msg acceptButton:(NSString*) ok acceptBlock:(ButtonCallback) acceptBlock;
+(void) alertWithTitle:(NSString*) title message:(NSString*) msg acceptButton:(NSString*) ok acceptBlock:(ButtonCallback) acceptBlock cancelButton:(NSString*) cancel cancelBlock:(ButtonCallback) cancelBlock;

+(void) alertWithTitle:(NSString*) title message:(NSString*) msg withAlignment:(NSTextAlignment) alignment acceptButton:(NSString*) ok acceptBlock:(ButtonCallback) acceptBlock cancelButton:(NSString*) cancel cancelBlock:(ButtonCallback) cancelBlock;

+(void) alertError:(TSError*)error;
+(void) alertError:(TSError*)error acceptButton:(NSString*)ok acceptBlock:(ButtonCallback) acceptBlock;
+(void) alertError:(TSError*)error acceptButton:(NSString*)ok acceptBlock:(ButtonCallback) acceptBlock cancelButton:(NSString*)cancel cancelBlock:(ButtonCallback) cancelBlock;
@end

@interface TSNotifier (Notifications)

+(void) notify:(NSString*) message onView:(UIView*) view;
+(void) notify:(NSString*) message onView:(UIView*) view forTimeInterval:(TSNotificationTimeInterval) time;
+(void) notifyError:(TSError*) error onView:(UIView*) view;
+(void) notifyError:(TSError*) error onView:(UIView*) view forTimeInterval:(TSNotificationTimeInterval) time;
+(void) hideNotificationOnView:(UIView*) view;
@end

@interface TSNotifier (ProgressBars)


+(void) showProgressOnView:(UIView*)view withMessage:(NSString*)message;


+(void) hideProgressOnView:(UIView*)view;
@end
