//
//  TSUtils.h
//
//  Created by Adya on 13/01/2015.
//  Copyright (c) 2015 Adya. All rights reserved.
//


#ifndef UTILS
#define UTILS

/*
 *  Replaces NSNull object with plain nil value.
 */

#define nonNull(value) [TSUtils nonNull:value]


/*
 *  NSString shortcuts
 */
#define trim(string) [TSUtils trim:string]


#define isValidURL(url) [TSUtils isValidURL:url]

/*
 * UIColor shortcuts with int values (255-base)
 */
#define colorARGB(a,r,g,b) [TSUtils colorWithRed:r green:g blue:b alpha:a]

#define colorRGB(r,g,b) [TSUtils colorWithRed:r green:g blue:b]

/*
 *  Shortuct to application name
 */
#define APP_NAME [TSUtils appName]

#define IP_ADDRESS [TSUtils getIPAddress]

/*
 * Marks method as abstract
 */
#define AbstractMethod() @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)] userInfo:nil];

/*
 *  System Versioning Preprocessor Macros
 */

#define SYSTEM_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#endif

@interface TSUtils : NSObject
+(NSString*) formatDateTime:(NSString*)dateTime DEPRECATED_MSG_ATTRIBUTE("Is under reconstruction.");
+(NSString*) formatToLocalFromUTCDateTime:(NSString*)dateTime DEPRECATED_MSG_ATTRIBUTE("Is under reconstruction.");

+(NSString*) convertDate:(NSDate*)date toStringWithFormat:(NSString*) dateFormat;
+(NSDate*) convertString:(NSString*)date toDateWithFormat:(NSString*) dateFormat;

+(NSString*) formatDateString:(NSString*) string withFormat:(NSString*)sourceFormat toFormat:(NSString*) destFormat;

+(NSString*) trim:(NSString*) target;
+(BOOL) isValidURL:(NSString*) url;
+(BOOL) isValidEmail:(NSString*) email;

//  Shortuct to application name
+(NSString*) appName;

// Replaces NSNull object with plain nil
+(id) nonNull:(id)target;

// 255-base UIColor construuctor.
+(UIColor*) colorWithRed:(int) red green:(int) green blue:(int)blue;
+(UIColor*) colorWithRed:(int) red green:(int) green blue:(int)blue alpha:(int)alpha;

+(NSString*) getIPAddress DEPRECATED_MSG_ATTRIBUTE("Will be moved to separate SystemManager.");
@end
