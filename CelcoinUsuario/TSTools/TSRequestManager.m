
#import "TSRequestManager.h"
#import "TSError.h"
#import "AFHTTPRequestOperationManager.h"
#import "UIImageView+AFNetworking+Animated.h"
#import "TSUtils.h"




@interface TimeoutAFJSONRequestSerializer : AFJSONRequestSerializer

@property (nonatomic, assign) NSTimeInterval timeout;

-(id) initWithTimeout:(NSTimeInterval) timeout;
@end

@implementation TimeoutAFJSONRequestSerializer

-(id) initWithTimeout:(NSTimeInterval)timeout{
    self = [super init];
    if (self){
        self.timeout = timeout;
    }
    return self;
}

-(NSMutableURLRequest*) requestWithMethod:(NSString *)method URLString:(NSString *)URLString parameters:(NSDictionary *)parameters{
    NSMutableURLRequest* request = [super requestWithMethod:method URLString:URLString parameters:parameters];
    if (self.timeout > 0)
        [request setTimeoutInterval:self.timeout];
    return request;
}

@end

@interface TSRequestManager (DynamicHeaders)
    -(void) attachHeaders:(NSDictionary*) headers;
    -(void) detachHeaders:(NSDictionary*) headers;
@end

@implementation TSRequestManager


+ (id) sharedManager{
    static TSRequestManager* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{manager = [[self alloc] init];});
    return manager;
}

-(id) init{
    if (self = [super init]){
        [self initManager];
        [self prepareManager];
    }
    return self;
}

-(void) initManager{
    afManager = [AFHTTPRequestOperationManager manager];
    afManager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    afManager.securityPolicy.allowInvalidCertificates = YES;
    afManager.requestSerializer = [[TimeoutAFJSONRequestSerializer alloc] initWithTimeout:[self getRequestTimeout]];
    afManager.responseSerializer = [AFJSONResponseSerializer serializer];
    }

// Override this to set up headers and/or other manager settings.
-(void) prepareManager{}

-(void) requestImageFromURL:(NSString*)url forImageView:(UIImageView*) imageView withPlaceholder:(UIImage*) placeholder success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image))success
                    failure:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error))failure{
    [imageView setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeholder animated:NO success:success failure:failure];
}

-(void) requestImageFromURL:(NSString*)url forImageView:(UIImageView*) imageView withPlaceholder:(UIImage*) placeholder{
     [imageView setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeholder animated:NO];
}

-(void) requestImageFromURL:(NSString*)url forImageView:(UIImageView*) imageView withPlaceholder:(UIImage*) placeholder animated:(BOOL)animated{
    [imageView setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeholder animated:animated];
        
}

-(NSString*) buildUrl:(NSString*) serverUrl andRequest:(NSString*) requestUrl{
   return [NSString stringWithFormat:@"%@/%@", serverUrl, requestUrl];
}


-(void) POST:(NSString*)requestUrl
        withBody:(NSDictionary*) body
        andHeaders:(NSDictionary*) headers
        successBlock:(void(^)(AFHTTPRequestOperation *operation, id responseObject))success
        failureBlock:(void(^)(AFHTTPRequestOperation *operation, NSError *error)) failure{
    NSString* url = [self buildUrl:[self getServerUrl] andRequest:requestUrl];
    [self attachHeaders:headers];
    [afManager POST:url parameters:body
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [self detachHeaders:headers];
                if (success){
                    success(operation, responseObject);
                }
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [self detachHeaders:headers];
                if (failure){
                    failure(operation, error);
                }
            }
     ];
}

-(void) DELETE:(NSString*)requestUrl
    withBody:(NSDictionary*) body
  andHeaders:(NSDictionary*) headers
successBlock:(void(^)(AFHTTPRequestOperation *operation, id responseObject))success
failureBlock:(void(^)(AFHTTPRequestOperation *operation, NSError *error)) failure{
    NSString* url = [self buildUrl:[self getServerUrl] andRequest:requestUrl];
    [self attachHeaders:headers];
    [afManager DELETE:url parameters:body
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [self detachHeaders:headers];
                if (success){
                    success(operation, responseObject);
                }
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [self detachHeaders:headers];
                if (failure){
                    failure(operation, error);
                }
            }
     ];
}

-(void)POST:(NSString*)requestUrl
   withData:(NSData*) fileData
      named:(NSString*) fileName
withMimeType:(NSString*) mimeType
 andHeaders:(NSDictionary*) headers
successBlock:(void(^)(AFHTTPRequestOperation *operation, id responseObject))success
failureBlock:(void(^)(AFHTTPRequestOperation *operation, NSError *error)) failure{
    NSString* url = [self buildUrl:[self getServerUrl] andRequest:requestUrl];
    [self attachHeaders:headers];
    [afManager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:fileData name:[self getFilesParamName] fileName:fileName mimeType:mimeType];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (success){
            success(operation, responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure){
            failure(operation, error);
        }
    }];
}
@end

@implementation TSRequestManager (DynamicHeaders)

-(void) attachHeaders:(NSDictionary *)headers{
    for (id key in headers) {
        [self setValue:[headers objectForKey:key] forHeaderField:key];
    }
}
-(void) detachHeaders:(NSDictionary *)headers{
    for (id key in headers) {
        [self setValue:nil forHeaderField:key];
    }
}

@end

@implementation TSRequestManager (HelperMethods)
-(NSDictionary*) wrapWithEntity:(NSDictionary*) params{
    NSMutableDictionary* entity = [NSMutableDictionary new];
    NSError* err;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    NSString* json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [entity setObject:json forKey:[self getWrapperEntityName]];
    return [NSDictionary dictionaryWithDictionary:entity];
}

-(NSData*) formDataParams:(NSDictionary*) params{
    NSError* err;
    if (params == nil) return nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&err];
    return jsonData;
}

-(void) setValue:(NSString *)value forHeaderField:(NSString *)header{
    [afManager.requestSerializer setValue: value forHTTPHeaderField:header];
}

-(void) setAcceptTypes:(NSArray *)types{
    NSString* tps = [types componentsJoinedByString:@", "];
    [self setValue:tps forHeaderField:@"Accept"];
    afManager.responseSerializer.acceptableContentTypes = [afManager.responseSerializer.acceptableContentTypes setByAddingObjectsFromArray:types];
}

-(void) setContentTypes:(NSArray *)types{
    NSString* tps = [types componentsJoinedByString:@", "];
    [self setValue:tps forHeaderField:@"Content-Type"];
}

-(TSError*) parseError:(NSDictionary*) errorResponse{
    TSError* err = [TSError errorWithCode:(int)errorResponse[[self getErrorCodeParamName]] Title:APP_NAME andDescription:errorResponse[[self getErrorMessageParamName]]];
    return err;
}

-(TSError*) parseNSError:(NSError*) error{
    return [TSError errorWithError:error];
}


@end

@implementation TSRequestManager (MustBeImplemented)

-(BOOL) isSuccessfulResponse:(NSDictionary*) response{
    AbstractMethod()
}

-(NSString*) getServerUrl{
    AbstractMethod();
}

-(NSString*) getErrorMessageParamName{
    AbstractMethod();
}

-(NSString*) getFilesParamName{
    AbstractMethod();
}

-(NSString*) getResponseParamName{
    AbstractMethod()
}

-(NSString*) getErrorCodeParamName{
    AbstractMethod();
}

-(NSString*) getWrapperEntityName{
    AbstractMethod();
}


-(NSTimeInterval) getRequestTimeout{
    return 30;
}

@end