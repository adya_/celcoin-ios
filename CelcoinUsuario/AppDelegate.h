//
//  AppDelegate.h
//  CelcoinUsuario
//
//  Created by Adya on 6/21/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

