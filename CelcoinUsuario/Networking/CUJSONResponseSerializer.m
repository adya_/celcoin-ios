//
//  CUJSONResponseSerializer.m
//  CelcoinUsuario
//
//  Created by Adya on 7/12/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CUJSONResponseSerializer.h"

@implementation CUJSONResponseSerializer

#warning Brut hack to fix server bug with "null" appended to each response. If this bug will be solved you should use base serializer instead of this.
-(id) responseObjectForResponse:(NSURLResponse *)response data:(NSData *)data error:(NSError *__autoreleasing *)error{
    
    NSMutableData* mData = [[NSMutableData alloc] initWithData:data];
    mData.length = mData.length - 4;// word "null"
    return [super responseObjectForResponse:response data:mData error:error];
}
@end
