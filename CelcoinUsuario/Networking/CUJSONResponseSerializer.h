//
//  CUJSONResponseSerializer.h
//  CelcoinUsuario
//
//  Created by Adya on 7/12/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

// This one used to deal with server-side bug returning "null" at the end of response

#import "AFURLResponseSerialization.h"

@interface CUJSONResponseSerializer : AFJSONResponseSerializer

@end
