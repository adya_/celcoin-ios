//
//  CURequestManager.h
//  CelcoinUsuario
//
//  Created by Adya on 7/6/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSRequestManager.h"


@interface CURequestManager : TSRequestManager

@end

@interface CURequestManager (User)

// returns "User" object which contains tokens
-(void) requestLoginForUserWithPhone:(NSString*)phone andPassword:(NSString*) password withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestLogoutForUserWithToken:(NSString*)token andAccessToken:(NSString*)accessToken withResponseCallback:(OperationResponseCallback) callback;

// returns "Registration" object which contains data for registration confirmation.
-(void) requestRegisterUserWithPhone:(NSString*)phone password:(NSString*) password SMSPassword:(NSString*)sms andSMSDate:(NSString*) smsDate withResponseCallback:(OperationResponseCallback)callback;

// completes registration.
-(void) requestCompleteUserRegistrationWithOperatorId:(int)operatorId forUserWithToken:(NSString*)token andAccessToken:(NSString*)accessToken withResponseCallback:(OperationResponseCallback) callback;

-(void) requestCompleteUserRegistrationWithName:(NSString*)name andCPF:(NSString*)cpf forUserWithToken:(NSString*)token andAccessToken:(NSString*)accessToken withResponseCallback:(OperationResponseCallback) callback;

// initiates sending password via SMS to confirm user.
-(void) requestUserConfirmSMS:(NSString*)phoneNumber withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestVerifySMSPassword:(NSString*)smsPassword andSMSDate:(NSString*) smsDate forPhoneNumber:(NSString*) phoneNumber withResponseCallback:(OperationResponseCallback) callback;

// returns array of "Transactions"
-(void) requestUserTransactionHistoryInRangeFrom:(NSString*) fromDate to:(NSString*) toDate forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ArrayResponseCallback) callback;

// returns information about referenced bank.
-(void) requestUserGetBankInfoForUserWithToken:(NSString*)token andAccessToken:(NSString*)accessToken withResponseCallback:(ObjectResponseCallback) callback;

// updates information about referenced bank.
-(void) requestUserSetBankInfoWithNickname:(NSString*) name withBankID:(int) bankID agency:(int) agency account:(int) account verificationDigit:(NSString*) verification bankReferenceID:(int) refId bankAccountType:(int) type registrationId:(int) regId forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(OperationResponseCallback) callback;
// returns list of strings with names of banks.
-(void) requestGetBankListForUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ArrayResponseCallback) callback;

-(void) requestRegistrationInfoForUserWithToken:(NSString*)token andAccessToken:(NSString*)accessToken withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestCheckContacts:(NSArray*) phoneNumbers forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ArrayResponseCallback) callback;

@end

@interface CURequestManager (Wallet)

-(void) requestGetWalletForUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ArrayResponseCallback) callback;

-(void) requestAvailableRechargeValuesForPhoneWithNumber:(NSString*) phone andOperatorCode:(int) operatorCode forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ArrayResponseCallback) callback;

-(void) requestRechargePhoneWithNumber:(NSString*) phone operator:(int) operatorCode withValue:(float) value forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestWithdrawal:(float)value withBankID:(int) bankID agency:(int) agency account:(int) account verificationDigit:(NSString*) verification bankAccountType:(int) type forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestCheckPaymentCode:(NSString*) code forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestPayment:(float) value withCode:(NSString*) code andDate:(NSString*) date forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestRedeemCouponWithCode:(NSString*) code forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ObjectResponseCallback) callback;

-(void) requestTransfer:(float)value fromNumber:(NSString*) srcNumber to:(NSString*) destNumber forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ObjectResponseCallback) callback;
@end
