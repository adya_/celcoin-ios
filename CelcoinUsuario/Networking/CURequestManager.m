//
//  CURequestManager.m
//  CelcoinUsuario
//
//  Created by Adya on 7/6/15.
//  Copyright (c) 2015 Adya. All rights reserved.
//

#import "CURequestManager.h"
#import "TSUtils.h"
#import "CUUser.h"
#import "AFHTTPRequestOperation.h"
#import "CUJSONResponseSerializer.h"
#import "AFHTTPRequestOperationManager.h"
#import "CUBankInfo.h"
#import "CUTransaction.h"
#import "TSError.h"
#import "CUPaymentReview.h"
#import "CUUserInfo.h"


#define SERVER_URL_LIVE @"https://services.is2b.com.br:53001/"
#define SERVER_URL @"http://hmlgtodaconta.is2b.com.br:53001/"
#define RESPONSE_PARAM_NAME @""
#define ERROR_CODE_PARAM_NAME @"Status"
#define ERROR_MSG_PARAM_NAME @"Mensagem"

#define RESULT_SUCCESS 0
#define RESULT_ERROR 1
#define RESULT_UNAUTHORIZED 2

//** GERAL **//

// Get current balance
#define REQUEST_GET_SALDO @"CelCoinWSUsuario/v1.0/Router/GetSaldo"

#define REQUEST_TRANSFERENCIA @"CelcoinWSTransacao/v1.0/Router/TransactionManager/TransferenciaCelcoin"
#define REQUEST_REVIEW_PAGAMENTO_CONTA @"CelcoinWSTransacao/v1.0/Router/TransactionManager/ConsultaConta"

#define REQUEST_PAGAMENTO_CONTA @"CelcoinWSTransacao/v1.0/Router/TransactionManager/PagamentoConta"

#define REQUEST_GET_CADASTRO @"CelCoinWSUsuario/v1.0/Router/GetCadastroUsuario"
//** USUARIOS **//

// User Login
#define REQUEST_LOGIN_USUARIO @"CelCoinWSUsuario/v1.0/Router/LoginUsuario"
// User Logout
#define REQUEST_LOGOUT_USUARIO @"CelCoinWSUsuario/v1.0/Router/Logout"
// User Register
#define REQUEST_CADASTRO_USUARIO @"CelCoinWSUsuario/v1.0/Router/CadastroUsuario"
// Finish User Register
#define REQUEST_COMPLETAR_CADASTRO_USUARIO @"CelCoinWSUsuario/v1.0/Router/CompletarCadastroUsuario"
// Text Message Password
#define REQUEST_SENHA_SMS @"CelCoinWSUsuario/v1.0/Router/SenhaSMS"

#define REQUEST_VERIFY_SENHA_SMS @"CelCoinWSUsuario/v1.0/Router/ValidaTokenSMS"

// User Transaction History
#define REQUEST_EXTRATO_USUARIO @"CelCoinWSUsuario/v1.0/Router/ExtratoUsuario"
// Get Bank reference info
#define REQUEST_GET_REFERENCIA_BANCARIA @"CelCoinWSUsuario/v1.0/Router/GetReferenciaBancaria"

#define REQUEST_SAQUE_BANCO @"CelcoinWSTransacao/v1.0/Router/TransactionManager/SaqueBanco"

// Set Bank reference info
#define REQUEST_SET_REFERENCIA_BANCARIA @"CelCoinWSUsuario/v1.0/Router/SetReferenciaBancaria"
// Get List of available banks
#define REQUEST_GET_BANCOS @"CelCoinWSUsuario/v1.0/Router/GetBancos"

#define REQUEST_ATIVAR_TROCO @"CelcoinWSTransacao/v1.0/Router/TransactionManager/ResgateTrocoExpresso"


#define REQUEST_RECARGA_SELULAR @"CelcoinWSTransacao/v1.0/Router/TransactionManager/RecargaCelular"

#define REQUEST_GET_RECARGA_VALUES @"CelcoinWSTransacao/v1.0/Router/TransactionManager/ConsultaValores"

#define REQUEST_CHECK_USER_CONTACTS @"CelCoinWSUsuario/v1.0/Router/ConsultaTelefoneCelcoin"

typedef NS_ENUM(NSInteger, CUResponseStatus){
    CU_RESPONSE_SUCCESS = 0,
    CU_RESPONSE_ERROR = 1,
    CU_RESPONSE_UNAUTHORIZED = 2
};

@implementation CURequestManager (MustBeImplemented)

-(NSString*) getServerUrl
{
#warning Here is the place to switch between LIVE and TEST environments
    return SERVER_URL_LIVE;
}

-(NSString*) getResponseParamName
{
    return RESPONSE_PARAM_NAME;
}

-(NSString*) getErrorCodeParamName
{
    return ERROR_CODE_PARAM_NAME;
}

-(NSString*) getErrorMessageParamName
{
    return ERROR_MSG_PARAM_NAME;
}

-(BOOL) isSuccessfulResponse:(NSDictionary*) response
{
    if (!nonNull(response)) return NO;
    NSInteger status = [[nonNull(response) valueForKey:[self getErrorCodeParamName]] integerValue];
    return status == CU_RESPONSE_SUCCESS;
}

@end

@implementation CURequestManager

-(void) prepareManager{
    afManager.responseSerializer = [CUJSONResponseSerializer serializer];
    [self setAcceptTypes: @[@"application/json"]];
    [self setContentTypes: @[@"application/json"]];
}

@end

@implementation CURequestManager (Logging)

-(void) POST:(NSString *)requestUrl withBody:(NSDictionary *)body andHeaders:(NSDictionary *)headers successBlock:(void (^)(AFHTTPRequestOperation *, id))success failureBlock:(void (^)(AFHTTPRequestOperation *, NSError *))failure{
    NSLog(@"Request: %@/%@", [self getServerUrl], requestUrl);
    NSLog(@"Body:\n%@", body);
    [super POST:requestUrl withBody:body andHeaders:headers
            successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"Response Success:\n%@", responseObject);
                if (success)
                    success(operation, responseObject);
            }
            failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Response Failed:\n%@", error.userInfo);
                if (failure)
                    failure(operation, error);
    }];

}

@end

@implementation CURequestManager (User)

-(void) requestLoginForUserWithPhone:(NSString*)phone andPassword:(NSString*) password withResponseCallback:(ObjectResponseCallback) callback
{
    NSDictionary* body = @{@"TelefoneEmail" : phone,
                           @"Senha" : password };
    [self POST:REQUEST_LOGIN_USUARIO withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [[CUUser alloc] initWithJSON:nonNull(responseObject)],nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", operation.responseString);
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestLogoutForUserWithToken:(NSString*)token andAccessToken:(NSString*)accessToken withResponseCallback:(OperationResponseCallback) callback
{
    NSDictionary* body = @{@"AuthenticationToken" : @{@"Token" : token,
                             @"AccessToken" : accessToken }};
    
    [self POST:REQUEST_LOGOUT_USUARIO withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}


-(void) requestRegisterUserWithPhone:(NSString*)phone password:(NSString*) password SMSPassword:(NSString*)sms andSMSDate:(NSString*) smsDate withResponseCallback:(OperationResponseCallback)callback
{
    NSDictionary* body = @{@"Telefone" : phone,
                           @"Senha" : password,
                           @"SenhaSMS" : sms,
                           @"DataGeracaoSenha" : smsDate};

    [self POST:REQUEST_CADASTRO_USUARIO withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}


-(void) requestCompleteUserRegistrationWithOperatorId:(int)operatorId forUserWithToken:(NSString*)token andAccessToken:(NSString*)accessToken withResponseCallback:(OperationResponseCallback) callback
{
    NSDictionary* body = @{@"OperadoraId" : @(operatorId),
                           @"AuthenticationToken" : @{@"Token" : token,
                                                      @"AccessToken" : accessToken }};
    [self POST:REQUEST_COMPLETAR_CADASTRO_USUARIO withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestCompleteUserRegistrationWithName:(NSString*)name andCPF:(NSString*)cpf forUserWithToken:(NSString*)token andAccessToken:(NSString*)accessToken withResponseCallback:(OperationResponseCallback) callback{
    NSDictionary* body = @{@"Nome" : name,
                           @"Cpf" : cpf,
                           @"AuthenticationToken" : @{@"Token" : token,
                                                      @"AccessToken" : accessToken }};
    [self POST:REQUEST_COMPLETAR_CADASTRO_USUARIO withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}

-(void) requestUserConfirmSMS:(NSString*)phoneNumber withResponseCallback:(ObjectResponseCallback) callback
{
    NSDictionary* body = @{@"Telefone" : phoneNumber};
    [self POST:REQUEST_SENHA_SMS withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [responseObject objectForKey:@"DataGeracao"], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestVerifySMSPassword:(NSString*)smsPassword andSMSDate:(NSString*) smsDate forPhoneNumber:(NSString*) phoneNumber withResponseCallback:(OperationResponseCallback) callback{
    NSDictionary* body = @{@"DadosToken":
                               @{@"Telefone" : phoneNumber,
                                 @"DataGeracao" : smsDate,
                                 @"TipoToken" : @(0)
                                 },
                           @"SenhaSMS" : smsPassword
                           };
    [self POST:REQUEST_VERIFY_SENHA_SMS withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject])
            callback(YES,nil);
        else
            callback(NO, [self parseError:responseObject]);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];
}


-(void) requestUserGetBankInfoForUserWithToken:(NSString*)token andAccessToken:(NSString*)accessToken withResponseCallback:(ObjectResponseCallback) callback
{
    NSDictionary* body = @{@"Token" : token,
                           @"AccessToken" : accessToken };
    [self POST:REQUEST_GET_REFERENCIA_BANCARIA withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [[CUBankInfo alloc] initWithJSON:nonNull([responseObject objectForKey:@"ReferenciaBancaria"])], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}


-(void) requestUserSetBankInfoWithNickname:(NSString*) name withBankID:(int) bankID agency:(int) agency account:(int) account verificationDigit:(NSString*) verification bankReferenceID:(int) refId bankAccountType:(int) type registrationId:(int) regId forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(OperationResponseCallback) callback
{
    NSDictionary* body = @{@"AuthenticationToken" : @{@"Token" : token,
                                                      @"AccessToken" : accessToken },
                           @"BancoId" : @(bankID),
                           @"Agencia" : @(agency),
                           @"Conta" : @(account),
                           @"DigitoVerificador" : verification,
                           @"Descricao" : name,
                           @"TipoContaBancaria" : @(type),
                           @"CadastroId" : @(regId),
                           @"ReferenciaBancariaId" : @(refId)
                           };
    [self POST:REQUEST_SET_REFERENCIA_BANCARIA withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nil);
        }
        else{
            callback(NO, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, [self parseNSError:error]);
    }];

}

-(void) requestGetBankListForUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ArrayResponseCallback) callback
{
    NSDictionary* body = @{@"Token" : token,
                           @"AccessToken" : accessToken };
    [self POST:REQUEST_GET_BANCOS withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nonNull([responseObject objectForKey:@"ListaString"]), nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestUserTransactionHistoryInRangeFrom:(NSString*) fromDate to:(NSString*) toDate forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ArrayResponseCallback) callback{
    NSDictionary* body = @{@"DataInicio" : fromDate,
                           @"DataFim" : toDate,
                           @"AuthenticationToken" : @{@"Token" : token,
                                                      @"AccessToken" : accessToken }};
    [self POST:REQUEST_EXTRATO_USUARIO withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, nonNull([responseObject objectForKey:@"Lancamento"]), nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestRegistrationInfoForUserWithToken:(NSString*)token andAccessToken:(NSString*)accessToken withResponseCallback:(ObjectResponseCallback) callback{
    NSDictionary* body = @{@"AuthenticationToken" : @{@"Token" : token,
                                                      @"AccessToken" : accessToken }};
    [self POST:REQUEST_GET_CADASTRO withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            callback(YES, [[CUUserInfo alloc] initWithJSON:nonNull([responseObject objectForKey:@"Cadastro"])], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestCheckContacts:(NSArray*) phoneNumbers forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ArrayResponseCallback) callback{
    NSDictionary* body = @{@"AuthenticationToken" : @{@"Token" : token,
                                                      @"AccessToken" : accessToken },
                           @"Telefones" : phoneNumbers};
    [self POST:REQUEST_CHECK_USER_CONTACTS withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){
            NSArray* phones = [responseObject objectForKey:@"Telefones"];
            NSMutableArray* celcoinPhones = [NSMutableArray new];
            for (NSDictionary* phone in phones) {
                if ([[phone objectForKey:@"Value"] intValue] == 1)
                    [celcoinPhones addObject:[phone objectForKey:@"Key"]];
            }
            callback(YES, [NSArray arrayWithArray:celcoinPhones], nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

@end

//
// Get wallet,
// Spent some,
// Refill.
//
@implementation CURequestManager (Wallet)

-(void) requestGetWalletForUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ArrayResponseCallback) callback
{
    NSDictionary* body = @{@"AuthenticationToken" : @{@"Token" : token,
                                                      @"AccessToken" : accessToken }};
    [self POST:REQUEST_GET_SALDO withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        if ([self isSuccessfulResponse:responseObject]){            
            callback(YES, nonNull([responseObject objectForKey:@"Carteira"]), nil);
        }
        else{
            callback(NO, nil, [self parseError:responseObject]);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}


-(void) requestAvailableRechargeValuesForPhoneWithNumber:(NSString*) phone andOperatorCode:(int) operatorCode forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ArrayResponseCallback) callback
{
    int transID = arc4random_uniform(999999) + 1;
    NSDictionary* body = @{@"oAuth" : @{@"Token" : token,
                                        @"AccessToken" : accessToken },
                           @"enderecoIp" : IP_ADDRESS,
                           @"operadoraId" : @(operatorCode),
                           @"codigoPais" : [phone substringWithRange:NSMakeRange(0, 3)],
                           @"codigoEstado" : [phone substringWithRange:NSMakeRange(3, 2)] ,
                           @"transacaoIdExterno" : @(transID),
                           @"tipoTransacao" : @"GETRECHARGEVALUES",
                           @"versao": @"v1.0",
                           @"descricaoCurta" : @"",
                           @"codigoMoeda": @"986",
                           @"codigoLoja": @"APPUSUARIO"};
    [self POST:REQUEST_GET_RECARGA_VALUES withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        BOOL success = !nonNull([responseObject objectForKey:@"mensagemErro"]); // if no error message than its success
        TSError* err = nil;
        if (!success)
            err = [TSError errorWithCode:0 Title:nil andDescription:[responseObject objectForKey:@"mensagemErro"]];
        callback(success, nonNull([responseObject objectForKey:@"valores"]), err);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];

}


-(void) requestRechargePhoneWithNumber:(NSString*) phone operator:(int) operatorCode withValue:(float) value forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ObjectResponseCallback) callback{
    int transID = arc4random_uniform(999999) + 1;
    NSDictionary* body = @{@"oAuth" : @{@"Token" : token,
                                        @"AccessToken" : accessToken },
                           @"enderecoIp" : IP_ADDRESS,
                           @"operadoraId" : @(operatorCode),
                           @"codigoPais" : [phone substringWithRange:NSMakeRange(0, 3)],
                           @"codigoEstado" : [phone substringWithRange:NSMakeRange(3, 2)] ,
                           @"numeroRecarga" : [phone substringFromIndex:3],
                           @"valor" : @(value),
                           @"transacaoIdExterno" : @(transID),
                           @"tipoTransacao" : @"PHONERECHARGE",
                           @"versao": @"v1.0",
                           @"descricaoCurta" : @"",
                           @"codigoMoeda": @"986",
                           @"codigoLoja": @"APPUSUARIO"};
    [self POST:REQUEST_RECARGA_SELULAR withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        BOOL success = !nonNull([responseObject objectForKey:@"mensagemErro"]); // if no error message than its success
        TSError* err = nil;
        if (!success)
            err = [TSError errorWithCode:0 Title:nil andDescription:[responseObject objectForKey:@"mensagemErro"]];
        callback(success, nonNull([responseObject objectForKey:@"mensagemComprovante"]), err);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
    
}


-(void) requestWithdrawal:(float)value withBankID:(int) bankID agency:(int) agency account:(int) account verificationDigit:(NSString*) verification bankAccountType:(int) type forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ObjectResponseCallback) callback{
    int transID = arc4random_uniform(999999) + 1;
    NSDictionary* body = @{@"oAuth" : @{@"Token" : token,
                                        @"AccessToken" : accessToken },
                           @"enderecoIp" : IP_ADDRESS,
                           @"transacaoIdExterno" : @(transID),
                           @"tipoTransacao" : @"WITHDRAWALTOBANK",
                           @"descricaoCurta" : @"Saque banco",
                           @"versao": @"v1.0",
                           @"codigoMoeda": @"986",
                           @"codigoLoja": @"APPUSUARIO",
                           @"valor" : @(value),
                           @"confirmacaoAutomatica" : @(YES),
                           @"bancoId" : @(bankID),
                           @"agencia" : @(agency),
                           @"conta" : @(account),
                           @"digitoVerificador" : verification,
                           @"tipoContaBancariaId" : @(type)
                           };
    [self POST:REQUEST_SAQUE_BANCO withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        BOOL success = !nonNull([responseObject objectForKey:@"mensagemErro"]); // if no error message than its success
        TSError* err = nil;
        if (!success)
            err = [TSError errorWithCode:0 Title:nil andDescription:[responseObject objectForKey:@"mensagemErro"]];
        callback(success, nonNull([responseObject objectForKey:@"mensagemComprovante"]), err);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestCheckPaymentCode:(NSString *)code forUserWithToken:(NSString *)token andAccessToken:(NSString *)accessToken withResponseCallback:(ObjectResponseCallback)callback{
    int transID = arc4random_uniform(999999) + 1;
    NSDictionary* body = @{@"oAuth" : @{@"Token" : token,
                                        @"AccessToken" : accessToken },
                           @"enderecoIp" : IP_ADDRESS,
                           @"transacaoIdExterno" : @(transID),
                           @"tipoTransacao" : @"GETBILLSDATA",
                           @"descricaoCurta" : @"Pagto. conta",
                           @"versao" : @"v1.0",
                           @"codigoMoeda" : @"986",
                           @"codigoLoja" : @"APPUSUARIO",
                           @"linhaDigitavel" : code,
                           };
    [self POST:REQUEST_REVIEW_PAGAMENTO_CONTA withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        BOOL success = !nonNull([responseObject objectForKey:@"mensagemErro"]); // if no error message than its success
        TSError* err = nil;
        CUPaymentReview* review = nil;
        if (!success)
            err = [TSError errorWithCode:0 Title:nil andDescription:[responseObject objectForKey:@"mensagemErro"]];
        else
            review = [[CUPaymentReview alloc] initWithJSON:nonNull(responseObject)];
        callback(success, review, err);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestPayment:(float) value withCode:(NSString*) code andDate:(NSString*) date forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ObjectResponseCallback) callback
{
    int transID = arc4random_uniform(999999) + 1;
    NSDictionary* body = @{@"oAuth" : @{@"Token" : token,
                                        @"AccessToken" : accessToken },
                           @"enderecoIp" : IP_ADDRESS,
                           @"transacaoIdExterno" : @(transID),
                           @"tipoTransacao" : @"BILLPAYMENT",
                           @"descricaoCurta" : @"Pagto. conta",
                           @"versao" : @"v1.0",
                           @"codigoMoeda" : @"986",
                           @"codigoLoja" : @"APPUSUARIO",
                           @"linhaDigitavel" : code,
                           @"dataVencimento" : date,
                           @"valor" : @(value)
                           };
    
    [self POST:REQUEST_PAGAMENTO_CONTA withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        BOOL success = !nonNull([responseObject objectForKey:@"mensagemErro"]); // if no error message than its success
        TSError* err = nil;
        if (!success)
            err = [TSError errorWithCode:0 Title:nil andDescription:[responseObject objectForKey:@"mensagemErro"]];
        callback(success, nonNull([responseObject objectForKey:@"mensagemComprovante"]), err);

    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}

-(void) requestRedeemCouponWithCode:(NSString*) code forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ObjectResponseCallback) callback{
    int transID = arc4random_uniform(999999) + 1;
    NSDictionary* body = @{@"oAuth" : @{@"Token" : token,
                                        @"AccessToken" : accessToken },
                           @"enderecoIp" : IP_ADDRESS,
                           @"transacaoIdExterno" : @(transID),
                           @"tipoTransacao" : @"REDEEMCHANGEEXPRESS",
                           @"descricaoCurta" : @"Ativ. troco",
                           @"versao" : @"v1.0",
                           @"codigoMoeda" : @"986",
                           @"codigoLoja" : @"APPUSUARIO",
                           @"codigoVoucher" : code
                           };
    [self POST:REQUEST_ATIVAR_TROCO withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        BOOL success = !nonNull([responseObject objectForKey:@"mensagemErro"]); // if no error message than its success
        TSError* err = nil;
        if (!success)
            err = [TSError errorWithCode:0 Title:nil andDescription:[responseObject objectForKey:@"mensagemErro"]];
        callback(success, nonNull([responseObject objectForKey:@"mensagemComprovante"]), err);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
    
}

-(void) requestTransfer:(float)value fromNumber:(NSString*) srcNumber to:(NSString*) destNumber forUserWithToken:(NSString*) token andAccessToken:(NSString*) accessToken withResponseCallback:(ObjectResponseCallback) callback
{
    int transID = arc4random_uniform(999999) + 1;
    NSDictionary* body = @{@"oAuth" : @{@"Token" : token,
                                        @"AccessToken" : accessToken },
                           @"enderecoIp" : IP_ADDRESS,
                           @"transacaoIdExterno" : @(transID),
                           @"tipoTransacao" : @"MONEYTRANSFER",
                           @"descricaoCurta" : @"Transferencia",
                           @"versao" : @"v1.0",
                           @"codigoMoeda" : @"986",
                           @"codigoLoja" : @"APPUSUARIO",
                           @"valor" : @(value),
                           @"telefoneTitular" : [srcNumber substringFromIndex:3],
                           @"telefoneTarget" : [destNumber substringFromIndex:3]
                           };
    [self POST:REQUEST_TRANSFERENCIA withBody:body andHeaders:nil successBlock:^(AFHTTPRequestOperation *operation, id responseObject) {
        BOOL success = !nonNull([responseObject objectForKey:@"mensagemErro"]); // if no error message than its success
        TSError* err = nil;
        if (!success)
            err = [TSError errorWithCode:0 Title:nil andDescription:[responseObject objectForKey:@"mensagemErro"]];
        callback(success, nonNull([responseObject objectForKey:@"mensagemComprovante"]), err);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        callback(NO, nil, [self parseNSError:error]);
    }];
}


@end

